VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmBooksAE 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Record"
   ClientHeight    =   5400
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5250
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5400
   ScaleWidth      =   5250
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSMask.MaskEdBox txtPrice 
      Height          =   285
      Left            =   1800
      TabIndex        =   5
      Top             =   3000
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   503
      _Version        =   393216
      MaxLength       =   9
      Format          =   "#,##0.00;(#,##0.00)"
      PromptChar      =   "_"
   End
   Begin VB.ComboBox cmbCategory 
      Height          =   315
      ItemData        =   "frmBooksAE.frx":0000
      Left            =   1800
      List            =   "frmBooksAE.frx":0046
      Sorted          =   -1  'True
      TabIndex        =   4
      Text            =   "N/A"
      Top             =   2640
      Width           =   2895
   End
   Begin MSMask.MaskEdBox msdISBN 
      Height          =   285
      Left            =   1800
      TabIndex        =   6
      Top             =   3360
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   503
      _Version        =   393216
      MaxLength       =   13
      Mask            =   "#-###-#####-C"
      PromptChar      =   "_"
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4080
      TabIndex        =   9
      Top             =   4875
      Width           =   1095
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      Height          =   375
      Left            =   2880
      TabIndex        =   8
      Top             =   4875
      Width           =   1095
   End
   Begin VB.CommandButton cmdAddSave 
      Caption         =   "Update"
      Default         =   -1  'True
      Height          =   375
      Left            =   1680
      TabIndex        =   7
      Top             =   4875
      Width           =   1095
   End
   Begin VB.TextBox txtTitle 
      Height          =   285
      Left            =   1800
      MaxLength       =   50
      TabIndex        =   1
      Top             =   1560
      Width           =   2895
   End
   Begin VB.TextBox txtAuthor 
      Height          =   285
      Left            =   1800
      MaxLength       =   50
      TabIndex        =   2
      Top             =   1920
      Width           =   2535
   End
   Begin VB.TextBox txtPublisher 
      Height          =   285
      Left            =   1800
      MaxLength       =   50
      TabIndex        =   3
      Top             =   2280
      Width           =   2895
   End
   Begin MSMask.MaskEdBox msdID 
      Height          =   285
      Left            =   1800
      TabIndex        =   0
      Top             =   1200
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   503
      _Version        =   393216
      MaxLength       =   10
      Mask            =   "B#########"
      PromptChar      =   "_"
   End
   Begin VB.Label Label11 
      Caption         =   $"frmBooksAE.frx":013F
      Height          =   855
      Left            =   1800
      TabIndex        =   21
      Top             =   3720
      Width           =   2895
   End
   Begin VB.Label Label10 
      Caption         =   "Borrowed:"
      Height          =   255
      Left            =   240
      TabIndex        =   20
      Top             =   3720
      Width           =   1095
   End
   Begin VB.Label Label9 
      Caption         =   "ISBN:"
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   3360
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   "Price:"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   3000
      Width           =   1095
   End
   Begin VB.Line Line3 
      BorderColor     =   &H80000014&
      X1              =   120
      X2              =   5160
      Y1              =   4755
      Y2              =   4755
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   $"frmBooksAE.frx":01E3
      Height          =   855
      Left            =   840
      TabIndex        =   17
      Top             =   120
      Width           =   4335
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Stretch         =   -1  'True
      Top             =   240
      Width           =   480
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   120
      X2              =   5040
      Y1              =   975
      Y2              =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Book ID:"
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "Title:"
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "Author:"
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Label Label5 
      Caption         =   "Publisher:"
      Height          =   255
      Left            =   240
      TabIndex        =   13
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label Label6 
      Caption         =   "Category:"
      Height          =   255
      Left            =   240
      TabIndex        =   12
      Top             =   2640
      Width           =   1215
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Index           =   0
      Left            =   4800
      TabIndex        =   11
      Top             =   1200
      Width           =   105
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   195
      Index           =   2
      Left            =   4800
      TabIndex        =   10
      Top             =   1560
      Width           =   105
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      BorderWidth     =   2
      X1              =   120
      X2              =   5040
      Y1              =   975
      Y2              =   975
   End
   Begin VB.Line Line4 
      BorderColor     =   &H80000010&
      BorderWidth     =   2
      X1              =   120
      X2              =   5160
      Y1              =   4755
      Y2              =   4755
   End
End
Attribute VB_Name = "frmBooksAE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'-------------------------------------------------------------------
'Books Add/Edit Form
'This form it is possible to add/modify the books table.
'
'AddState is the bool value that decides whether the form will update
'or modify a record. When AddState = True then Add
'                    When AddState = False then Modify
'-------------------------------------------------------------------
Public AddState As Boolean, OldID As String
Private rs As ADODB.RecordSet

Private Sub Form_Load()

'Prepare form to add/edit. This is done because we are reusing the form.

    On Error GoTo Err
    Set rs = New ADODB.RecordSet
    If AddState Then
        Image1.Picture = frmBooks.cmdAMod(1).Picture
        rs.Open "SELECT * FROM tblBooks", CN, adOpenStatic, adLockOptimistic
        Me.Caption = "Add Record"
    Else 'NOT AddState...
        Image1.Picture = frmBooks.cmdAMod(0).Picture
        Me.Caption = "Modify Record"
        cmdAddSave.Caption = "Save"
        rs.Open "SELECT * FROM tblBooks WHERE [Book ID] = '" & OldID & "'", CN, adOpenStatic, adLockOptimistic
    End If

Exit Sub

Err:
    If Err.Number = 94 Or Err.Number = 3265 Then
        Resume Next '-If encounter a null value
    Else
        Handler Err '-Unexpected error
    End If

End Sub

Private Sub cmdAddSave_Click()

'Add or edit according to AddState

    On Error GoTo hell
    'Verify
    If msdID.Text = "" Then msdID.SetFocus: Exit Sub
    If txtTitle.Text = "" Then txtTitle.SetFocus: Exit Sub
    If Len(msdID.Text) <> 10 Then MsgBox "All Book ID must be 10 charecters long", vbExclamation: HighLight msdID: Exit Sub
    msdID.Text = UCase$(msdID.Text)
    If IsNumeric(Right$(msdID.Text, 9)) = False Then MsgBox "Book ID must start with B followed by 9 digits", vbExclamation: HighLight msdID: Exit Sub

    If AddState Then
        If RecordExists("tblBooks", "Book ID", msdID.Text, msdID) = True Then Exit Sub
    Else 'NOT AddState...
        If msdID.Text <> OldID Then
            If RecordExists("tblBooks", "Book ID", msdID.Text, msdID) = True Then Exit Sub
        End If
    End If

    CN.BeginTrans
    With rs
        If AddState = True Then rs.AddNew
        .Fields(0) = msdID.Text
        .Fields(1) = txtTitle.Text
        .Fields(4) = cmbCategory.Text
        .Fields(5) = CCur(txtPrice.Text)
        .Fields(6) = msdISBN.Text

        If txtAuthor.Text = "" Then .Fields(2) = " " Else .Fields(2) = txtAuthor.Text
        If txtPublisher.Text = "" Then .Fields(3) = " " Else .Fields(3) = txtPublisher.Text
        If txtPrice.Text = "" Then txtPrice.Text = "0"
        rs.Update
    End With
    CN.CommitTrans

    If AddState Then
        FindRecord rs, rs.Fields(0).Name, True, msdID.Text, 0
        MsgBox "New record has been successfully added", vbInformation

        If MsgBox("Do you want to add a new record?", vbQuestion + vbYesNo + vbDefaultButton1) = vbYes Then
            cmdReset_Click
        Else
            Unload Me
        End If

    Else 'NOT AddState...
        FindRecord rs, rs.Fields(0).Name, True, msdID.Text, 0

        MsgBox "Changes in record has been successfully saved", vbInformation
        Unload Me
    End If

Exit Sub

hell:
    On Error Resume Next
        Handler Err
        CN.RollbackTrans

End Sub

Private Sub cmdCancel_Click()

    Unload Me

End Sub

Private Sub cmdReset_Click()

'Reset all values to nothing/null/empty/nullstring/0

    msdID.Text = ""
    txtTitle.Text = ""
    txtAuthor.Text = ""
    txtPublisher.Text = ""
    txtPrice.Text = ""
    msdISBN.Text = ""
    cmbCategory.ListIndex = 0

End Sub
