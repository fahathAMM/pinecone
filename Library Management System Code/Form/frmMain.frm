VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm frmMain 
   BackColor       =   &H8000000C&
   Caption         =   "Sunnydale Library Management System"
   ClientHeight    =   7035
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   9165
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   600
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9165
      _ExtentX        =   16166
      _ExtentY        =   1058
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      Style           =   1
      ImageList       =   "ImgList32"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Issue Book"
            Object.ToolTipText     =   "Issue Book"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Return Book"
            Object.ToolTipText     =   "Return Book"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Book Records"
            Object.ToolTipText     =   "Book Records"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Member Records"
            Object.ToolTipText     =   "Member Records"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Reports"
            Object.ToolTipText     =   "Reports"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Settings"
            Object.ToolTipText     =   "Settings"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "About"
            Object.ToolTipText     =   "About"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   6780
      Width           =   9165
      _ExtentX        =   16166
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Currently Logged in user"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   2831
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            AutoSize        =   2
            Enabled         =   0   'False
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            AutoSize        =   2
            Enabled         =   0   'False
            TextSave        =   "CAPS"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   4
            AutoSize        =   2
            Enabled         =   0   'False
            TextSave        =   "SCRL"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   3
            AutoSize        =   2
            Enabled         =   0   'False
            TextSave        =   "INS"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImgList16 
      Left            =   8520
      Top             =   5520
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0E42
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImgList32 
      Left            =   8520
      Top             =   6120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":11DC
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1EB6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2D08
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":39E2
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":46BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":5396
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":6070
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":6D4A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuSettings 
         Caption         =   "&Settings"
      End
      Begin VB.Menu sep 
         Caption         =   "-"
         Index           =   0
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuTransaction 
      Caption         =   "Tr&ansaction"
      Begin VB.Menu mnuIssue 
         Caption         =   "&Issue"
      End
      Begin VB.Menu mnuReturn 
         Caption         =   "&Return"
      End
   End
   Begin VB.Menu mnuRecords 
      Caption         =   "&Records"
      Begin VB.Menu mnuBookRec 
         Caption         =   "&Book Record"
      End
      Begin VB.Menu mnuMembers 
         Caption         =   "&Members Record"
      End
   End
   Begin VB.Menu mnuReports 
      Caption         =   "R&eport"
      Begin VB.Menu mnuBookRep 
         Caption         =   "&Book Report"
      End
      Begin VB.Menu mnuReport 
         Caption         =   "&Student Report"
      End
      Begin VB.Menu mnuUnreturnedBooks 
         Caption         =   "&Unreturned Books"
      End
   End
   Begin VB.Menu mnuWindow 
      Caption         =   "&Windows"
      WindowList      =   -1  'True
      Begin VB.Menu mnuArrangeIcons 
         Caption         =   "&Arrange Icons"
      End
      Begin VB.Menu mnuCascade 
         Caption         =   "&Cascade"
      End
      Begin VB.Menu mnuTileHorizontal 
         Caption         =   "Tile &Horizontally"
      End
      Begin VB.Menu mnuTileVertical 
         Caption         =   "Tile &Vertically"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'----------------------------------------------------------------------
'Coursework for Half-Yearly Exams June 2005
'
'Source Code:
'Sunnydale Library Management System 1.0
'
'Devoloper:
'Hussain Mohd. Elius (www.elvista.cjb.net)
'Class IX, Cygnus, Roll: 5
'----------------------------------------------------------------------

'\/ This API Call and the manifest file together will make this program support Win XP
Private Declare Function InitCommonControls Lib "comctl32.dll" () As Long

Private Sub MDIForm_Load()

    Me.Show
    Set CN = New ADODB.Connection
    CN.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\MasterFile.mdb;Persist Security Info=False;"
    If CN.State <> adStateOpen Then MsgBox "Could not establish a connection with the database" & vbNewLine & "The database should exist in ApplicationPath\MasterFile.mdb", vbExclamation, "Database not found!": Unload Me
    frmReturn.FineAmnt = CCur(GetSetting(App.Title, "Settings", "Fine Amount", "2"))
    frmReturn.MaxDays = CInt(GetSetting(App.Title, "Settings", "Max Days", "14"))

End Sub

Private Sub MDIForm_Unload(Cancel As Integer)

Dim Form As Form

    For Each Form In Forms
        Unload Form
        Set Form = Nothing
    Next Form
    Set CN = Nothing

End Sub

Private Sub MDIForm_Initialize()
    InitCommonControls
End Sub

'---------------------------------------------------------------------
'=======================     Menu Coding    ==========================
'== Provides connectivity to other parts of the program to the user ==
'---------------------------------------------------------------------

Private Sub mnuAbout_Click()

    frmAbout.Show vbModal

End Sub

Private Sub mnuArrangeIcons_Click()

    frmMain.Arrange vbArrangeIcons

End Sub

Private Sub mnuBookRec_Click()

    With frmBooks
        .Show
        .SetFocus
    End With

End Sub

Private Sub mnuBookRep_Click()

Dim RS As New ADODB.RecordSet

    RS.Open "SELECT * FROM tblBooks Order by [Book ID]", CN, adOpenStatic, adLockReadOnly
    Set drBookList.DataSource = RS
    drBookList.Show
    Set RS = Nothing

End Sub

Private Sub mnuCascade_Click()

    frmMain.Arrange vbCascade

End Sub

Private Sub mnuIssue_Click()

    frmIssue.Show vbModal

End Sub

Private Sub mnuMembers_Click()

    With frmMembers
        .Show
        .SetFocus
    End With

End Sub

Private Sub mnuReport_Click()

Dim RS As New ADODB.RecordSet

    RS.Open "SELECT * FROM tblMembers Order by [Student ID]", CN, adOpenStatic, adLockReadOnly
    Set drMembers.DataSource = RS
    drMembers.Show
    Set RS = Nothing

End Sub

Private Sub mnuReturn_Click()

    frmReturn.Show vbModal

End Sub

Private Sub mnuSettings_Click()

    frmSettings.Show vbModal

End Sub

Private Sub mnuTileHorizontal_Click()

    frmMain.Arrange vbTileHorizontal

End Sub

Private Sub mnuTileVertical_Click()

    frmMain.Arrange vbTileVertical

End Sub

Private Sub mnuExit_Click()

    Unload Me

End Sub

Private Sub mnuUnreturnedBooks_Click()

Dim RS As New ADODB.RecordSet

    RS.Open "SELECT tblTrans.[Book ID], tblTrans.[Student ID], tblBooks.Title, [First Name] & ' ' & [Middle Initial] & ' ' & [Last Name] AS Borrower, tblTrans.[Date Borrowed] FROM tblMembers INNER JOIN (tblBooks INNER JOIN tblTrans ON tblBooks.[Book ID] = tblTrans.[Book ID]) ON tblMembers.[Student ID] = tblTrans.[Student ID] Where (((tblTrans.Returned) = False)) ORDER BY tblTrans.[Book ID];", CN, adOpenStatic, adLockReadOnly
    Set drTransUn.DataSource = RS
    drTransUn.Show
    Set RS = Nothing

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Index
    Case 1: mnuIssue_Click
    Case 2: mnuReturn_Click
    Case 4: mnuBookRec_Click
    Case 5: mnuMembers_Click
    Case 6: PopupMenu mnuReports, , Toolbar1.Buttons(6).Left, Toolbar1.Top + Toolbar1.Height
    Case 8: mnuSettings_Click
    Case 9: mnuAbout_Click
    End Select

End Sub
