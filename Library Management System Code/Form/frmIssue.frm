VERSION 5.00
Begin VB.Form frmIssue 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Books Issue Form"
   ClientHeight    =   4200
   ClientLeft      =   4860
   ClientTop       =   3855
   ClientWidth     =   5295
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   5295
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Text6 
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   3120
      Width           =   2895
   End
   Begin VB.CommandButton cmdBook 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4770
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Browse for record"
      Top             =   2040
      Width           =   315
   End
   Begin VB.CommandButton cmdCode 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4770
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "Browse record."
      Top             =   1320
      Width           =   315
   End
   Begin VB.TextBox Text5 
      BackColor       =   &H80000018&
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   2040
      Width           =   2535
   End
   Begin VB.TextBox Text4 
      BackColor       =   &H80000018&
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   1320
      Width           =   2535
   End
   Begin VB.TextBox Text3 
      BackColor       =   &H8000000E&
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   2760
      Width           =   2895
   End
   Begin VB.TextBox Text2 
      BackColor       =   &H00F4FEFF&
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   2400
      Width           =   2895
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00F4FEFF&
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   1680
      Width           =   3015
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   375
      Left            =   1920
      TabIndex        =   4
      Top             =   3720
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   3720
      TabIndex        =   3
      Top             =   3720
      Width           =   1455
   End
   Begin VB.CommandButton cmdIssue 
      Caption         =   "&Issue Book"
      Default         =   -1  'True
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   3720
      Width           =   1455
   End
   Begin VB.Label Label6 
      Caption         =   "Date to be returned:"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   3120
      Width           =   1695
   End
   Begin VB.Label Label5 
      Caption         =   "Date Issued:"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   2760
      Width           =   1695
   End
   Begin VB.Label Label4 
      Caption         =   "Book Title:"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   2400
      Width           =   1695
   End
   Begin VB.Label Label3 
      Caption         =   "Student Name:"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   $"frmIssue.frx":0000
      Height          =   855
      Index           =   1
      Left            =   840
      TabIndex        =   5
      Top             =   120
      Width           =   4335
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Stretch         =   -1  'True
      Top             =   240
      Width           =   480
   End
   Begin VB.Label Label2 
      Caption         =   "Book ID:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   2040
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Student Code:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   1575
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      Index           =   1
      X1              =   120
      X2              =   5160
      Y1              =   3600
      Y2              =   3600
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      BorderWidth     =   2
      Index           =   1
      X1              =   120
      X2              =   5160
      Y1              =   3600
      Y2              =   3600
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      Index           =   0
      X1              =   120
      X2              =   5040
      Y1              =   975
      Y2              =   975
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      BorderWidth     =   2
      Index           =   0
      X1              =   120
      X2              =   5040
      Y1              =   975
      Y2              =   975
   End
End
Attribute VB_Name = "frmIssue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'----------------------------------------------------------------------
'Books Issue Form
'This form is used to issue books that are present in the library.
'and recorded in the database via the issue form.
'More details in cmdCode_Click and cmdReturn_Click procedures.
'-----------------------------------------------------------------------

Private Sub cmdBook_Click()

    With frmSelectDg
        .CommandText = "Select * From tblBooks where Borrowed=False"
        .DataGrid1.Caption = "Members Table"
        .Show vbModal
        If .OKPressed Then
            Text5.Text = .rRS1
            Text2.Text = .rRS2
        End If
    End With

End Sub

Private Sub cmdCancel_Click()

    Unload Me

End Sub

Private Sub cmdCode_Click()

Dim a As String, b As String, c As String

    With frmSelectDg
        .CommandText = "Select * From tblMembers"
        .DataGrid1.Caption = "Members Table"
        .Show vbModal
        If .OKPressed Then
            Text4.Text = .rRS1
            a = .rRS2
            b = .rRS3
            c = .rRS4
            Text1.Text = a & " " & b & " " & c
        End If
    End With

End Sub

Private Sub cmdIssue_Click()

'Record that the book was taken in two places. In tblTrans, and in
'tblBooks which will set the Borrowed Boolean to True.

Dim rs As ADODB.RecordSet

    If Text4.Text = "" Then Text4.SetFocus: Exit Sub
    If Text5.Text = "" Then Text5.SetFocus: Exit Sub
    On Error GoTo hell
    CN.BeginTrans
    Set rs = New ADODB.RecordSet
    With rs
        .Open "Select * from tblTrans", CN, adOpenDynamic, adLockOptimistic
        .AddNew
        .Fields(0) = Text5.Text
        .Fields(1) = Text4.Text
        .Fields(2) = Date
        .Update
        .Close

        .Open "Select [Borrowed] from tblBooks where [Book ID]='" & Text5.Text & "'", CN, adOpenDynamic, adLockOptimistic
        .MoveFirst
        .Fields(0) = True
        .Update
        .Close
        Set rs = Nothing
    End With
    CN.CommitTrans
    If MsgBox("The book " & Text5.Text & " has been issued to " & Text4.Text & vbNewLine & "Do you want to create a new issue instance?", vbInformation + vbYesNo) = vbYes Then
        cmdReset_Click
    Else
        Unload Me
    End If

Exit Sub

hell:
    Handler Err
    CN.RollbackTrans

End Sub

Private Sub cmdReset_Click()

    Text1.Text = ""
    Text2.Text = ""
    Text5.Text = ""
    Text4.Text = ""
    Text3.Text = FormatDateTime$(Date, vbLongDate)
    Text6.Text = FormatDateTime$(Date + frmReturn.MaxDays, vbLongDate)

End Sub

Private Sub Form_Load()

    cmdReset_Click
    With frmMain
        cmdCode.Picture = .ImgList16.ListImages(1).Picture
        Me.Icon = .ImgList32.ListImages(7).Picture
    End With
    cmdBook.Picture = cmdCode.Picture
    Image1.Picture = Me.Icon

End Sub

Private Sub Text4_Keypress(Keyascii As Integer)

    cmdCode_Click

End Sub

Private Sub Text5_KeyPress(Keyascii As Integer)

    cmdBook_Click

End Sub
