-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 14, 2016 at 12:26 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `curriculumdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `Username` varchar(15) NOT NULL,
  `Password` varchar(15) NOT NULL,
  PRIMARY KEY (`Password`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Username`, `Password`) VALUES
('JEN', 'YUI'),
('true', 'false'),
('try', 'try'),
('', '');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_table`
--

CREATE TABLE IF NOT EXISTS `faculty_table` (
  `FIRSTNAME` varchar(50) NOT NULL,
  `LASTNAME` varchar(50) NOT NULL,
  `MI` varchar(1) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `CONFIRM` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`CONFIRM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_table`
--

INSERT INTO `faculty_table` (`FIRSTNAME`, `LASTNAME`, `MI`, `USERNAME`, `PASSWORD`, `CONFIRM`) VALUES
('Berlyn', 'Madagsen', 'P', 'Blue', '12345', '12345'),
('JOY', 'PET', 'L', 'JOY', 'JIP', 'JIP'),
('gigi', 'dangelan', 'r', 'try', 'try', 'tryy');

-- --------------------------------------------------------

--
-- Table structure for table `grade_table`
--

CREATE TABLE IF NOT EXISTS `grade_table` (
  `ID_NO` varchar(12) NOT NULL,
  `COURSE_NO` varchar(50) NOT NULL,
  `GRADE` float NOT NULL,
  PRIMARY KEY (`ID_NO`,`COURSE_NO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_table`
--

INSERT INTO `grade_table` (`ID_NO`, `COURSE_NO`, `GRADE`) VALUES
('427222', 'NAT SCI 12', 0),
('427222', 'SOC SCI 12', 0),
('427222', 'MATH 13', 0),
('427222', 'ENG 12', 0),
('427222', 'FIL 11', 0),
('427222', 'NSTP 11', 0),
('427222', 'PE 11', 0),
('427222', 'IT 102', 0),
('427222', 'IT 101', 0),
('427222', 'NAT SCI 11', 0),
('427222', 'SOC SCI 11', 0),
('427222', 'MATH 11', 0),
('427222', 'ENG 11', 0),
('427', 'BUS COM 11', 0),
('427', 'PE 13', 0),
('427', 'IT 108', 0),
('427', 'IT 107', 0),
('427', 'IT 106', 0),
('427', 'IT 105', 0),
('427', 'IT 104', 0),
('427', 'HIST 11', 0),
('427', 'FIL 13', 0),
('427', 'PE 14', 0),
('427', 'IT 111', 0),
('427', 'IT 110', 0),
('427', 'IT 109', 0),
('33333', 'FIL 12', 0),
('33333', 'ENG 13', 0),
('33333', 'STAT 12', 0),
('33333', 'SOC SCI 13', 0),
('33333', 'HIST 12', 0),
('33333', 'IT 109', 0),
('33333', 'IT 110', 0),
('33333', 'IT 111', 0),
('33333', 'PE 14', 0),
('33333', 'FIL 13', 0),
('33333', 'HIST 11', 0),
('33333', 'IT 104', 0),
('33333', 'IT 105', 0),
('33333', 'IT 106', 0),
('33333', 'IT 107', 0),
('33333', 'IT 108', 0),
('33333', 'PE 13', 0),
('33333', 'BUS COM 11', 0),
('427', 'ENG 11', 0),
('427', 'MATH 11', 0),
('427', 'SOC SCI 11', 0),
('427', 'NAT SCI 11', 0),
('427', 'IT 101', 0),
('427', 'IT 102', 0),
('427', 'PE 11', 0),
('427', 'NSTP 11', 0),
('427', 'FIL 11', 0),
('427', 'ENG 12', 0),
('427', 'MATH 13', 0),
('427', 'SOC SCI 12', 0),
('427', 'NAT SCI 12', 0),
('427', 'IT 103', 0),
('427', 'HUM 11', 0),
('427', 'PE 12', 0),
('427', 'NSTP 12', 0),
('427', 'FIL 12', 0),
('427', 'ENG 13', 0),
('427', 'STAT 12', 0),
('427', 'SOC SCI 13', 0),
('427', 'HIST 12', 0),
('6276', 'BUS COM 11', 0),
('6276', 'PE 13', 0),
('6276', 'IT 108', 0),
('6276', 'IT 107', 0),
('6276', 'IT 106', 0),
('6276', 'IT 105', 0),
('6276', 'IT 104', 0),
('6276', 'HIST 11', 0),
('6276', 'FIL 13', 0),
('6276', 'PE 14', 0),
('6276', 'IT 111', 0),
('6276', 'IT 110', 0),
('6276', 'IT 109', 0),
('6276', 'HIST 12', 0),
('6276', 'SOC SCI 13', 0),
('6276', 'STAT 12', 0),
('6276', 'ENG 13', 0),
('6276', 'FIL 12', 0),
('6276', 'NSTP 12', 0),
('6276', 'PE 12', 0),
('6276', 'HUM 11', 0),
('6276', 'IT 103', 0),
('6276', 'NAT SCI 12', 0),
('6276', 'SOC SCI 12', 0),
('6276', 'MATH 13', 0),
('6276', 'ENG 12', 0),
('6276', 'FIL 11', 0),
('6276', 'NSTP 11', 0),
('6276', 'PE 11', 0),
('6276', 'IT 102', 0),
('6276', 'IT 101', 0),
('6276', 'NAT SCI 11', 0),
('6276', 'SOC SCI 11', 0),
('6276', 'MATH 11', 0),
('6276', 'ENG 11', 0),
('262777', 'BUS COM 11', 0),
('262777', 'PE 13', 0),
('262777', 'IT 108', 0),
('262777', 'IT 107', 0),
('262777', 'IT 106', 0),
('262777', 'IT 105', 0),
('262777', 'IT 104', 0),
('262777', 'HIST 11', 0),
('262777', 'FIL 13', 0),
('262777', 'PE 14', 0),
('262777', 'IT 111', 0),
('262777', 'IT 110', 0),
('262777', 'IT 109', 0),
('262777', 'HIST 12', 0),
('262777', 'SOC SCI 13', 0),
('262777', 'STAT 12', 0),
('262777', 'ENG 13', 0),
('262777', 'FIL 12', 0),
('262777', 'NSTP 12', 0),
('262777', 'PE 12', 0),
('262777', 'HUM 11', 0),
('262777', 'IT 103', 0),
('262777', 'NAT SCI 12', 0),
('262777', 'SOC SCI 12', 0),
('262777', 'MATH 13', 0),
('262777', 'ENG 12', 0),
('262777', 'FIL 11', 0),
('262777', 'NSTP 11', 0),
('262777', 'PE 11', 0),
('262777', 'IT 102', 0),
('262777', 'IT 101', 0),
('262777', 'NAT SCI 11', 0),
('262777', 'SOC SCI 11', 0),
('262777', 'MATH 11', 0),
('262777', 'ENG 11', 0),
('444444', 'BUS COM 11', 0),
('444444', 'PE 13', 0),
('444444', 'IT 108', 0),
('444444', 'IT 107', 0),
('444444', 'IT 106', 0),
('444444', 'IT 105', 0),
('444444', 'IT 104', 0),
('444444', 'HIST 11', 0),
('444444', 'FIL 13', 0),
('444444', 'PE 14', 0),
('444444', 'IT 111', 0),
('444444', 'IT 110', 0),
('444444', 'IT 109', 0),
('444444', 'HIST 12', 0),
('444444', 'SOC SCI 13', 0),
('444444', 'STAT 12', 0),
('444444', 'ENG 13', 0),
('444444', 'FIL 12', 0),
('444444', 'NSTP 12', 0),
('444444', 'PE 12', 0),
('444444', 'HUM 11', 0),
('444444', 'IT 103', 0),
('444444', 'NAT SCI 12', 0),
('444444', 'SOC SCI 12', 0),
('444444', 'MATH 13', 0),
('444444', 'ENG 12', 0),
('444444', 'FIL 11', 0),
('444444', 'NSTP 11', 0),
('444444', 'PE 11', 0),
('444444', 'IT 102', 0),
('444444', 'IT 101', 0),
('444444', 'NAT SCI 11', 0),
('444444', 'SOC SCI 11', 0),
('444444', 'MATH 11', 0),
('444444', 'ENG 11', 0),
('427222', 'BUS COM 11', 0),
('427222', 'PE 13', 0),
('427222', 'IT 108', 0),
('427222', 'IT 107', 0),
('427222', 'IT 106', 0),
('427222', 'IT 105', 0),
('427222', 'IT 104', 0),
('427222', 'HIST 11', 0),
('427222', 'FIL 13', 0),
('427222', 'PE 14', 0),
('427222', 'IT 111', 0),
('427222', 'IT 110', 0),
('427222', 'IT 109', 0),
('427222', 'HIST 12', 0),
('427222', 'SOC SCI 13', 0),
('427222', 'STAT 12', 0),
('427222', 'ENG 13', 0),
('427222', 'FIL 12', 0),
('427222', 'NSTP 12', 0),
('427222', 'PE 12', 0),
('427222', 'IT 103', 0),
('427222', 'HUM 11', 0),
('33333', 'NSTP 12', 0),
('33333', 'PE 12', 0),
('33333', 'HUM 11', 0),
('33333', 'IT 103', 0),
('33333', 'NAT SCI 12', 0),
('33333', 'SOC SCI 12', 0),
('33333', 'MATH 13', 0),
('33333', 'ENG 12', 0),
('33333', 'FIL 11', 2),
('33333', 'NSTP 11', 2),
('33333', 'PE 11', 2),
('33333', 'IT 102', 2),
('33333', 'IT 101', 2),
('33333', 'NAT SCI 11', 2),
('33333', 'SOC SCI 11', 2),
('33333', 'MATH 11', 2),
('33333', 'ENG 11', 2),
('6518', 'IT 102', 3),
('6518', 'IT 101', 3),
('6518', 'NAT SCI 11', 3),
('6518', 'SOC SCI 11', 3),
('6518', 'MATH 11', 3),
('6518', 'ENG 11', 3);

-- --------------------------------------------------------

--
-- Table structure for table `prerequisite_table`
--

CREATE TABLE IF NOT EXISTS `prerequisite_table` (
  `COURSE_NO` varchar(15) NOT NULL,
  `PREREQUISITE` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`COURSE_NO`,`PREREQUISITE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prerequisite_table`
--

INSERT INTO `prerequisite_table` (`COURSE_NO`, `PREREQUISITE`) VALUES
('BUS COM 11', 'ENG 12'),
('eeee', 'eeeeeeeeeeee'),
('ENG 12', 'ENG 11'),
('ENG 13', 'ENG 13'),
('FIL 11', 'None'),
('FIL 12', 'IT 102'),
('FIL 13', 'FIL 12'),
('IT 104', 'IT 103'),
('IT 105', 'IT 103'),
('IT 106', 'MATH 13'),
('IT 108', 'IT 101'),
('IT 109', 'IT 102, IT 106'),
('IT 110', 'IT 108'),
('IT 111', 'IT 108'),
('IT 112', '3RD YEAR STANDING'),
('IT 113', '3RD YEAR STANDING'),
('IT 114', '3RD YEAR STANDING'),
('IT 115', '3RD YEAR STANDING'),
('IT 116', '3RD YEAR STANDING'),
('IT 117', 'IT 114, IT 115'),
('IT 118', '3RD YEAR STANDING'),
('IT 119', 'IT 101'),
('IT 120', '3RD YEAR STANDING'),
('IT 121', '4TH YEAR STANDING'),
('IT 122', '3RD YEAR STANDING'),
('IT 123', '4TH YEAR STANDING'),
('IT 124', '4TH YEAR STANDING'),
('IT 125', '4TH YEAR STANDING'),
('IT 126', '3RD YEAR STANDING'),
('IT 128', '4TH YEAR STANDING'),
('IT 129', '4TH YEAR STANDING'),
('IT 132', '4TH YEAR STANDING'),
('NAT SCI 16', 'NAT SCI 12'),
('NSTP 12', 'NSTP 11'),
('PE 12', 'PE 11'),
('PE 13', 'PE 11'),
('PE 14', 'PE 11'),
('PHILO 11', 'HUM 11'),
('SOC SCI 13', 'SOC SCI 12'),
('SOC SCI 14', 'SOC SCI 13'),
('STAT 12', 'MATH 13'),
('try111', ''),
('TRY6', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `student_table`
--

CREATE TABLE IF NOT EXISTS `student_table` (
  `ID_NO` varchar(12) NOT NULL,
  `LASTNAME` varchar(20) NOT NULL,
  `FIRSTNAME` varchar(20) NOT NULL,
  `MI` varchar(1) NOT NULL,
  `COURSE` varchar(40) NOT NULL,
  `ADDRESS` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_NO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_table`
--

INSERT INTO `student_table` (`ID_NO`, `LASTNAME`, `FIRSTNAME`, `MI`, `COURSE`, `ADDRESS`) VALUES
('6518', 't', 't', 't', 't', 't'),
('3334', 'e', 'e', 'e', 'e', 'e'),
('1626', '6', '6', '6', '6', '6'),
('4115', 'd', 'd', 'd', 'd', 'd'),
('1267', 'f', 'f', 'f', 'f', 'ff'),
('689', 'f', 'f', 'f', 'f', 'f'),
('627', 'f', 'f', 'f', 'f', 'f'),
('165', 'F', 'F', 'F', 'FF', 'F'),
('1526', 'f', 'f', 'f', 'f', 'f'),
('44444', '4', '4', '4', '4', '4'),
('222', '2', '2', '2', '2', '2'),
('4', 'd', 'd', 'd', 'd', 'd'),
('1555', 'e', 'e', 'e', 'e', 'e'),
('111111', 'w', 'w', 'w', 'w', 'w'),
('1425', 'f', 'f', 'f', 'f', 'ff'),
('55555', 'f', 'f', 'f', 'f', 'f'),
('178', 't', 't', 't', 't', 't'),
('168', 'r', 'r', 'r', 'rr', 'r'),
('1234', 't', 't', 't', 'tt', 't'),
('819', 'n', 'n', 'n', 'n', 'n'),
('23', 'q', 's', 's', 's', 's'),
('14', 'r', 'r', 'r', 'r', 'r'),
('167111', 'f', 'f', 'f', 'f', 'f'),
('167', 'f', 'f', 'f', 'f', 'f'),
('156', 'c', 'c', 'c', 'c', 'c'),
('415', 'd', 'd', 'd', 'd', 'd'),
('1672', 'f', 'f', 'f', 'f', 'f'),
('5166', 'dddd', 'd', 'd', 'd', 'd'),
('4156', 'rr', 'r', 'r', 'rr', 'r'),
('617', 't', 't', 't', 'tt', 't'),
('13', '', '', '', '', ''),
('', '', '', '', '', ''),
('61', 'g', 'g', 'g', 'g', 'g'),
('12333', 't', 't', 't', 't', 't'),
('99', 'j', 'j', 'j', 'j', 'j'),
('45555', 'g', 'g', 'g', 'g', 'g'),
('444', 'd', 'd', 'd', 'd', 'd'),
('1234567', 'h', 'h', 'h', 'h', 'h'),
('425', 'd', 'd', 'd', 'd', 'd'),
('2222', 'f', 'f', 'f', 'f', 'f'),
('123', 'ggG', 'g', 'g', 'gg', 'g'),
('12', 'ggG', 'g', 'g', 'gg', 'g'),
('4555565', 'hHh', 'h', 'h', 'h', 'h'),
('`123', 'rRr', 'RR', 'R', 'R', 'R'),
('23456790', 'pp', 'p', 'p', 'p', 'p'),
('12345', 'g', 'g', 'g', 'g', 'g'),
('213', 'w', 'w', 'w', 'w', 'w'),
('33333', '3', '3', '3', '3', '3'),
('427', 'f', 'f', 'f', 'f', 'f'),
('427222', 'f', 'f', 'f', 'f', 'f'),
('444444', '4', '4', '4', '4', '4'),
('262777', 'f', 'f', 'f', 'f', 'f'),
('6276', 'r', 'r', 'r', 'r', 'r');

-- --------------------------------------------------------

--
-- Table structure for table `subject_table`
--

CREATE TABLE IF NOT EXISTS `subject_table` (
  `COURSE_NO` varchar(12) NOT NULL,
  `DESCRIPTIVE_TITLE` varchar(60) NOT NULL,
  `LEC_UNIT` smallint(2) NOT NULL,
  `LAB_UNIT` smallint(2) NOT NULL,
  `SEMESTER` varchar(60) NOT NULL,
  `YEARLEVEL` varchar(60) NOT NULL,
  `AcademicYear` varchar(35) NOT NULL,
  `PreRequisite` varchar(255) NOT NULL,
  PRIMARY KEY (`COURSE_NO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_table`
--

INSERT INTO `subject_table` (`COURSE_NO`, `DESCRIPTIVE_TITLE`, `LEC_UNIT`, `LAB_UNIT`, `SEMESTER`, `YEARLEVEL`, `AcademicYear`, `PreRequisite`) VALUES
('ENG 11', 'STUDY AND THINKING SKILLS', 3, 0, 'First', 'First', '', ''),
('MATH 11', 'COLLEGE ALGEBRA', 3, 0, 'First', 'First', '', ''),
('SOC SCI 11', 'GENERAL PSYCHOLOGY WITH DRUG ABUSE PREVENTION', 3, 0, 'First', 'First', '', ''),
('NAT SCI 11', 'BIOLOGICAL SCIENCE', 3, 0, 'First', 'First', '', ''),
('IT 101', 'IT FUNDAMENTALS', 2, 1, 'First', 'First', '', ''),
('IT 102', 'PROGRAMMING LANGUAGE (LOGIC FORMULATION)', 2, 1, 'First', 'First', '', ''),
('NSTP 11', 'NATIONAL SERVICE TRAINING PROGRAM 11', 3, 0, 'First', 'First', '', ''),
('ENG 12', 'WRITING IN THE DISCIPLINE', 3, 0, 'Second', 'First', '', ''),
('FIL 12', 'PAGBASA AT PAGSULAT TUNGO SA PANANALIKSIK', 3, 0, 'Second', 'First', '', ''),
('MATH 13', 'PLANE AND SPHERICAL TRIGONOMETRY', 4, 0, 'Second', 'First', '', ''),
('NAT SCI 12', 'GENERAL PHYSICS ', 3, 0, 'Second', 'First', '', ''),
('SOC SCI 12', 'SOCIETY, CULTURE AND FAMILY PLANNING', 3, 0, 'Second', 'First', '', ''),
('HUM 11', 'INTRO TO HUMANITIES', 3, 0, 'Second', 'First', '', ''),
('IT 103', 'PROGRAMMING 2 (STRUCTURED COMP PROG)', 2, 1, 'Second', 'First', '', ''),
('PE 12', 'INDIVIDUAL AND DUAL SPORTS', 2, 0, 'Second', 'First', '', ''),
('NSTP 12', 'NATIONAL SERVICE TRAINING PROGRAM 2', 3, 0, 'Second', 'First', '', ''),
('BUS COM 11', 'BUSINESS COMMUNICATION', 3, 0, 'First', 'Second', '', ''),
('FIL 13', 'MASINING NA PAKIKIPAGTALASTASAN', 3, 0, 'First', 'Second', '', ''),
('HIST 11', 'LIFE AND WORKS OF RIZAL', 3, 0, 'First', 'Second', '', ''),
('IT 104', 'COMPUTER ORGANIZATION', 2, 1, 'First', 'Second', '', ''),
('IT 105', 'OBJECT ORIENTED PROGRAMMING', 2, 1, 'First', 'Second', '', ''),
('IT 106', 'DISCRETE MATHEMATICS', 3, 0, 'First', 'Second', '', ''),
('IT 107', 'ACCOUNTING PRINCIPLES AND FINANCIAL SYSTEM', 3, 0, 'First', 'Second', '', ''),
('IT 108', 'PC SERVICING', 2, 1, 'First', 'Second', '', ''),
('PE 13', 'RHYTHMIC ACTIVITIES', 2, 0, 'First', 'Second', '', ''),
('ENG 13', 'SPEECH AND ORAL COMMUNICATION', 3, 0, 'Second', 'Second', '', ''),
('STAT 12', 'PROBABILITY AND STATISTICS', 4, 0, 'Second', 'Second', '', ''),
('SOC SCI 13', 'BASIC ECON. W/ TAXATION AND AGRARIAN  REFORM', 3, 0, 'Second', 'Second', '', ''),
('HIST 12', 'Philippine History Its roots and Development', 3, 0, 'Second', 'Second', '', ''),
('IT 109', 'DATABASE MANAGEMENT SYSTEM', 2, 1, 'Second', 'Second', '', ''),
('IT 110', 'OPERATING SYSTEM APPLICATIONS', 2, 1, 'Second', 'Second', '', ''),
('IT 111', 'NETWORK MANAGEMENT', 2, 1, 'Second', 'Second', '', ''),
('PE 14', 'TEAM SPORTS', 2, 0, 'Second', 'Second', '', ''),
('PHILO 11', 'LOGIC AND PHILOSOPHY OF MAN', 3, 0, 'First', 'Third', '', ''),
('LIT 11', 'PHILIPPINE LITERATURE', 3, 0, 'First', 'Third', '', ''),
('IT 112', 'WEB DEVELOPMENT', 2, 1, 'First', 'Third', '', ''),
('IT 113', 'DATABASE MANAGEMENT SYSTEM 2', 2, 1, 'First', 'Third', '', ''),
('IT 114', 'SYSTEM ANALYSIS AND DESIGN', 3, 0, 'First', 'Third', '', ''),
('IT 115', 'IT ELECTIVE 1(FUTURE AND CURRENT TRENDS IN IT)', 3, 0, 'First', 'Third', '', ''),
('IT 116', 'SEMINARS/FIELTRIPS', 1, 0, 'First', 'Third', '', ''),
('SOC SCI 14', 'POLITICS, GOVERNANCE W/ THE NEW CONSTITUTION', 3, 0, 'First', 'Third', '', ''),
('LIT 12', 'WOLRD LITERATURE', 3, 0, 'Second', 'Third', '', ''),
('IT 117', 'SOFTWARE ENGINEERING', 2, 1, 'Second', 'Third', '', ''),
('IT 118', 'MULTIMEDIA SYSTEM', 2, 1, 'Second', 'Third', '', ''),
('IT 119', 'PROFESSIONAL ETHICS', 3, 0, 'Second', 'Third', '', ''),
('IT 120', 'IT ELECTIVE 2(NETWORK ADMIN AND PERFORMANCE)', 2, 1, 'Second', 'Third', '', ''),
('IT 126', 'CONTENT MANAGEMENT SYSTEM', 3, 0, 'Second', 'Third', '', ''),
('IT 122', 'CAPSTONE PROJECT 1', 2, 1, 'Second', 'Third', '', ''),
('NAT SCI 16', 'ELECTRICITY AND MAGNETISM ', 3, 0, 'Second', 'Third', '', ''),
('IT 123', 'CAPSTONE PROJECT 2', 2, 1, 'First', 'Fourth', '', ''),
('IT 124', 'IT ELECTIVE 3(WIRELESS TECH)', 2, 1, 'First', 'Fourth', '', ''),
('IT 125', 'IT ELECTIVE 4(JAVA PROGRAMMING)', 2, 1, 'First', 'Fourth', '', ''),
('IT 121', 'MOBILE COMPUTING', 3, 0, 'First', 'Fourth', '', ''),
('IT 132', 'AUTOCAD', 3, 0, 'First', 'Fourth', '', ''),
('IT 128', 'IT ELECTIVE 5(ARTIFICIAL INTELLIGENCE)', 2, 1, 'First', 'Fourth', '', ''),
('IT 129', 'OJT/INTERNSHIP PROGRAM(600 HOURS)', 0, 9, 'Second', 'Fourth', '', ''),
('FIL 11', 'KOMUNIKASYON SA AKADEMIKONG FILIPINO', 3, 0, 'First', 'First', '', ''),
('PE 11', 'PHYSICAL FITNESS & SELF TESTING ACTIVITIES', 2, 0, 'First', 'First', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblauto`
--

CREATE TABLE IF NOT EXISTS `tblauto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autostart` varchar(30) NOT NULL,
  `autoend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tblauto`
--

INSERT INTO `tblauto` (`id`, `autostart`, `autoend`) VALUES
(1, '100', 16),
(2, '0', 8);

-- --------------------------------------------------------

--
-- Table structure for table `tblcourse`
--

CREATE TABLE IF NOT EXISTS `tblcourse` (
  `CourseId` int(11) NOT NULL AUTO_INCREMENT,
  `Course` varchar(255) NOT NULL,
  PRIMARY KEY (`CourseId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tblcourse`
--

INSERT INTO `tblcourse` (`CourseId`, `Course`) VALUES
(1, 'BSIT'),
(2, 'BSEED');

-- --------------------------------------------------------

--
-- Table structure for table `tblcurriculum`
--

CREATE TABLE IF NOT EXISTS `tblcurriculum` (
  `CurriculumId` int(11) NOT NULL AUTO_INCREMENT,
  `SubjectId` int(11) NOT NULL,
  `CourseId` int(11) NOT NULL,
  `Semester` varchar(35) NOT NULL,
  `YearLevel` varchar(32) NOT NULL,
  `AcademicYear` varchar(30) NOT NULL,
  `PreRequisite` varchar(255) NOT NULL,
  PRIMARY KEY (`CurriculumId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tblcurriculum`
--

INSERT INTO `tblcurriculum` (`CurriculumId`, `SubjectId`, `CourseId`, `Semester`, `YearLevel`, `AcademicYear`, `PreRequisite`) VALUES
(1, 1, 1, 'First', 'First', '2016-2017', 'None'),
(2, 2, 1, 'First', 'First', '2016-2017', 'None'),
(3, 1, 1, 'First', 'Second', '2016-2017', 'ENG 11'),
(4, 1, 1, 'Second', 'First', '2016-2017', 'None'),
(5, 2, 2, 'First', 'First', '2016-2017', 'None'),
(6, 2, 2, 'First', 'Second', '2016-2017', 'MATH 11');

-- --------------------------------------------------------

--
-- Table structure for table `tblgrades`
--

CREATE TABLE IF NOT EXISTS `tblgrades` (
  `GradesId` int(11) NOT NULL AUTO_INCREMENT,
  `CourseId` int(11) NOT NULL,
  `IdNo` int(11) NOT NULL,
  `SubjectId` int(11) NOT NULL,
  `Grades` double NOT NULL,
  `YearLevel` varchar(32) NOT NULL,
  `Sem` varchar(99) NOT NULL,
  PRIMARY KEY (`GradesId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `tblgrades`
--

INSERT INTO `tblgrades` (`GradesId`, `CourseId`, `IdNo`, `SubjectId`, `Grades`, `YearLevel`, `Sem`) VALUES
(1, 1, 123123, 3, 88, 'First', 'First'),
(2, 1, 123123, 4, 88, 'First', 'First'),
(3, 1, 123123, 5, 88, 'First', 'First'),
(4, 1, 123123, 6, 88, 'First', 'First'),
(5, 1, 123123, 7, 88, 'First', 'First'),
(6, 1, 123123, 8, 88, 'First', 'First'),
(7, 1, 123123, 9, 88, 'First', 'First'),
(8, 1, 123123, 10, 78, 'First', 'Second'),
(9, 1, 123123, 11, 77, 'First', 'Second'),
(10, 1, 123123, 12, 76, 'First', 'Second'),
(11, 1, 123123, 13, 87, 'First', 'Second'),
(12, 1, 123123, 14, 88, 'First', 'Second'),
(13, 1, 123123, 15, 84, 'First', 'Second'),
(14, 1, 123123, 16, 76, 'First', 'Second'),
(15, 1, 123123, 17, 83, 'First', 'Second'),
(16, 1, 123123, 18, 81, 'First', 'Second'),
(17, 1, 123123, 19, 0, 'Second', 'First'),
(18, 1, 123123, 20, 0, 'Second', 'First'),
(19, 1, 123123, 21, 0, 'Second', 'First'),
(20, 1, 123123, 22, 0, 'Second', 'First'),
(21, 1, 123123, 23, 0, 'Second', 'First'),
(22, 1, 123123, 24, 0, 'Second', 'First'),
(23, 1, 123123, 25, 0, 'Second', 'First'),
(24, 1, 123123, 26, 0, 'Second', 'First'),
(25, 1, 123123, 27, 0, 'Second', 'First'),
(26, 1, 123123, 28, 0, 'Second', 'Second'),
(27, 1, 123123, 29, 0, 'Second', 'Second'),
(28, 1, 123123, 30, 0, 'Second', 'Second'),
(29, 1, 123123, 31, 0, 'Second', 'Second'),
(30, 1, 123123, 32, 0, 'Second', 'Second'),
(31, 1, 123123, 33, 0, 'Second', 'Second'),
(32, 1, 123123, 34, 0, 'Second', 'Second'),
(33, 1, 123123, 35, 0, 'Second', 'Second'),
(34, 1, 123123, 36, 0, 'Third', 'First'),
(35, 1, 123123, 37, 0, 'Third', 'First'),
(36, 1, 123123, 38, 0, 'Third', 'First'),
(37, 1, 123123, 39, 0, 'Third', 'First'),
(38, 1, 123123, 40, 0, 'Third', 'First'),
(39, 1, 123123, 41, 0, 'Third', 'First'),
(40, 1, 123123, 42, 0, 'Third', 'First'),
(41, 1, 123123, 43, 0, 'Third', 'First'),
(42, 1, 123123, 44, 0, 'Third', 'Second'),
(43, 1, 123123, 45, 0, 'Third', 'Second'),
(44, 1, 123123, 46, 0, 'Third', 'Second'),
(45, 1, 123123, 47, 0, 'Third', 'Second'),
(46, 1, 123123, 48, 0, 'Third', 'Second'),
(47, 1, 123123, 49, 0, 'Third', 'Second'),
(48, 1, 123123, 50, 0, 'Third', 'Second'),
(49, 1, 123123, 51, 0, 'Third', 'Second'),
(50, 1, 123123, 52, 0, 'Fourth', 'First'),
(51, 1, 123123, 53, 0, 'Fourth', 'First'),
(52, 1, 123123, 54, 0, 'Fourth', 'First'),
(53, 1, 123123, 55, 0, 'Fourth', 'First'),
(54, 1, 123123, 56, 0, 'Fourth', 'First'),
(55, 1, 123123, 57, 0, 'Fourth', 'First'),
(56, 1, 123123, 58, 0, 'Fourth', 'Second'),
(57, 1, 123123, 59, 0, 'Fourth', 'Second'),
(58, 1, 123123, 60, 0, 'Fourth', 'Second');

-- --------------------------------------------------------

--
-- Table structure for table `tblstudent`
--

CREATE TABLE IF NOT EXISTS `tblstudent` (
  `IdNo` int(11) NOT NULL AUTO_INCREMENT,
  `Firstname` varchar(255) NOT NULL,
  `Lastname` varchar(255) NOT NULL,
  `MI` varchar(255) NOT NULL,
  `HomeAddress` varchar(255) NOT NULL,
  `Sex` varchar(35) NOT NULL,
  `CourseId` int(11) NOT NULL,
  `YearLevel` varchar(32) NOT NULL,
  `StudentPhoto` varchar(255) NOT NULL,
  PRIMARY KEY (`IdNo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=312312313 ;

--
-- Dumping data for table `tblstudent`
--

INSERT INTO `tblstudent` (`IdNo`, `Firstname`, `Lastname`, `MI`, `HomeAddress`, `Sex`, `CourseId`, `YearLevel`, `StudentPhoto`) VALUES
(123123, 'Mark Antony', 'Fernandez', 'R', 'Kalinga Aklan', 'Male', 1, 'First', 'Chrysanthemum.jpg'),
(1234322, 'Angel', 'Alvarez', 'A', 'kalinga', 'Female', 1, 'First', 'Desert.jpg'),
(312312312, 'James', 'Yap', 'E', 'Aklan', 'Male', 1, 'Second', 'Desert.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tblsubject`
--

CREATE TABLE IF NOT EXISTS `tblsubject` (
  `SubjectId` int(11) NOT NULL AUTO_INCREMENT,
  `Subject` varchar(128) NOT NULL,
  `DescriptiveTitle` varchar(255) NOT NULL,
  `LecUnit` int(11) NOT NULL,
  `LabUnit` int(11) NOT NULL,
  `CourseId` int(11) NOT NULL,
  `YearLevel` varchar(30) NOT NULL,
  `Semester` varchar(30) NOT NULL,
  `AcademicYear` varchar(90) NOT NULL,
  `PreRequisite` varchar(255) NOT NULL,
  `Level` int(11) NOT NULL,
  `PreTaken` tinyint(1) NOT NULL,
  PRIMARY KEY (`SubjectId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `tblsubject`
--

INSERT INTO `tblsubject` (`SubjectId`, `Subject`, `DescriptiveTitle`, `LecUnit`, `LabUnit`, `CourseId`, `YearLevel`, `Semester`, `AcademicYear`, `PreRequisite`, `Level`, `PreTaken`) VALUES
(3, 'ENG 11', 'STUDY AND THINKING SKILLS', 3, 0, 1, 'First', 'First', '2016-2017', 'None', 1, 0),
(4, 'MATH 11', 'COLLEGE ALGEBRA', 3, 0, 1, 'First', 'First', '2016-2017', 'None', 1, 0),
(5, 'SOC SCI 11', 'GENERAL PSYCHOLOGY WITH DRUG ABUSE PREVENTION', 3, 0, 1, 'First', 'First', '2016-2017', 'None', 1, 0),
(6, 'NAT SCI 11', 'BIOLOGICAL SCIENCE', 3, 0, 1, 'First', 'First', '2016-2017', 'None', 1, 0),
(7, 'IT 101', 'IT FUNDAMENTALS', 2, 1, 1, 'First', 'First', '2016-2017', 'None', 1, 0),
(8, 'IT 102', 'PROGRAMMING LANGUAGE (LOGIC FORMULATION)', 2, 1, 1, 'First', 'First', '2016-2017', 'None', 1, 0),
(9, 'NSTP 11', 'NATIONAL SERVICE TRAINING PROGRAM 11', 3, 0, 1, 'First', 'First', '2016-2017', 'None', 1, 0),
(10, 'ENG 12', 'WRITING IN THE DISCIPLINE', 3, 0, 1, 'First', 'Second', '2016-2017', 'ENG 11', 1, 0),
(11, 'FIL 12', 'PAGBASA AT PAGSULAT TUNGO SA PANANALIKSIK', 3, 0, 1, 'First', 'Second', '2016-2017', 'None', 1, 0),
(12, 'MATH 13', 'PLANE AND SPHERICAL TRIGONOMETRY', 4, 0, 1, 'First', 'Second', '2016-2017', 'MATH 11', 1, 0),
(13, 'NAT SCI 12', 'GENERAL PHYSICS ', 3, 0, 1, 'First', 'Second', '2016-2017', 'None', 1, 0),
(14, 'SOC SCI 12', 'SOCIETY, CULTURE AND FAMILY PLANNING', 3, 0, 1, 'First', 'Second', '2016-2017', 'None', 1, 0),
(15, 'HUM 11', 'INTRO TO HUMANITIES', 3, 0, 1, 'First', 'Second', '2016-2017', 'None', 1, 0),
(16, 'IT 103', 'PROGRAMMING 2 (STRUCTURED COMP PROG)', 2, 1, 1, 'First', 'Second', '2016-2017', 'None', 1, 0),
(17, 'PE 12', 'INDIVIDUAL AND DUAL SPORTS', 2, 0, 1, 'First', 'Second', '2016-2017', 'None', 1, 0),
(18, 'NSTP 12', 'NATIONAL SERVICE TRAINING PROGRAM 2', 3, 0, 1, 'First', 'Second', '2016-2017', 'None', 1, 0),
(19, 'BUS COM 11', 'BUSINESS COMMUNICATION', 3, 0, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(20, 'FIL 13', 'MASINING NA PAKIKIPAGTALASTASAN', 3, 0, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(21, 'HIST 11', 'LIFE AND WORKS OF RIZAL', 3, 0, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(22, 'IT 104', 'COMPUTER ORGANIZATION', 2, 1, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(23, 'IT 105', 'OBJECT ORIENTED PROGRAMMING', 2, 1, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(24, 'IT 106', 'DISCRETE MATHEMATICS', 3, 0, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(25, 'IT 107', 'ACCOUNTING PRINCIPLES AND FINANCIAL SYSTEM', 3, 0, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(26, 'IT 108', 'PC SERVICING', 2, 1, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(27, 'PE 13', 'RHYTHMIC ACTIVITIES', 2, 0, 1, 'Second', 'First', '2016-2017', 'None', 2, 0),
(28, 'ENG 13', 'SPEECH AND ORAL COMMUNICATION', 3, 0, 1, 'Second', 'Second', '2016-2017', 'None', 2, 0),
(29, 'STAT 12', 'PROBABILITY AND STATISTICS', 4, 0, 1, 'Second', 'Second', '2016-2017', 'None', 2, 0),
(30, 'SOC SCI 13', 'BASIC ECON. W/ TAXATION AND AGRARIAN  REFORM', 3, 0, 1, 'Second', 'Second', '2016-2017', 'None', 2, 0),
(31, 'HIST 12', 'Philippine History Its roots and Development', 3, 0, 1, 'Second', 'Second', '2016-2017', 'None', 2, 0),
(32, 'IT 109', 'DATABASE MANAGEMENT SYSTEM', 2, 1, 1, 'Second', 'Second', '2016-2017', 'None', 2, 0),
(33, 'IT 110', 'OPERATING SYSTEM APPLICATIONS', 2, 1, 1, 'Second', 'Second', '2016-2017', 'None', 2, 0),
(34, 'IT 111', 'NETWORK MANAGEMENT', 2, 1, 1, 'Second', 'Second', '2016-2017', 'None', 2, 0),
(35, 'PE 14', 'TEAM SPORTS', 2, 0, 1, 'Second', 'Second', '2016-2017', 'None', 2, 0),
(36, 'PHILO 11', 'LOGIC AND PHILOSOPHY OF MAN', 3, 0, 1, 'Third', 'First', '2016-2017', 'None', 3, 0),
(37, 'LIT 11', 'PHILIPPINE LITERATURE', 3, 0, 1, 'Third', 'First', '2016-2017', 'None', 3, 0),
(38, 'IT 112', 'WEB DEVELOPMENT', 2, 1, 1, 'Third', 'First', '2016-2017', 'None', 3, 0),
(39, 'IT 113', 'DATABASE MANAGEMENT SYSTEM 2', 2, 1, 1, 'Third', 'First', '2016-2017', 'None', 3, 0),
(40, 'IT 114', 'SYSTEM ANALYSIS AND DESIGN', 3, 0, 1, 'Third', 'First', '2016-2017', 'None', 3, 0),
(41, 'IT 115', 'IT ELECTIVE 1(FUTURE AND CURRENT TRENDS IN IT)', 3, 0, 1, 'Third', 'First', '2016-2017', 'None', 3, 0),
(42, 'IT 116', 'SEMINARS/FIELTRIPS', 1, 0, 1, 'Third', 'First', '2016-2017', 'None', 3, 0),
(43, 'SOC SCI 14', 'POLITICS, GOVERNANCE W/ THE NEW CONSTITUTION', 3, 0, 1, 'Third', 'First', '2016-2017', 'None', 3, 0),
(44, 'LIT 12', 'WOLRD LITERATURE', 3, 0, 1, 'Third', 'Second', '2016-2017', 'None', 3, 0),
(45, 'IT 117', 'SOFTWARE ENGINEERING', 2, 1, 1, 'Third', 'Second', '2016-2017', 'None', 3, 0),
(46, 'IT 118', 'MULTIMEDIA SYSTEM', 2, 1, 1, 'Third', 'Second', '2016-2017', 'None', 3, 0),
(47, 'IT 119', 'PROFESSIONAL ETHICS', 3, 0, 1, 'Third', 'Second', '2016-2017', 'None', 3, 0),
(48, 'IT 120', 'IT ELECTIVE 2(NETWORK ADMIN AND PERFORMANCE)', 2, 1, 1, 'Third', 'Second', '2016-2017', 'None', 3, 0),
(49, 'IT 126', 'CONTENT MANAGEMENT SYSTEM', 3, 0, 1, 'Third', 'Second', '2016-2017', 'None', 3, 0),
(50, 'IT 122', 'CAPSTONE PROJECT 1', 2, 1, 1, 'Third', 'Second', '2016-2017', 'None', 3, 0),
(51, 'NAT SCI 16', 'ELECTRICITY AND MAGNETISM ', 3, 0, 1, 'Third', 'Second', '2016-2017', 'None', 3, 0),
(52, 'IT 123', 'CAPSTONE PROJECT 2', 2, 1, 1, 'Fourth', 'First', '2016-2017', 'None', 4, 0),
(53, 'IT 124', 'IT ELECTIVE 3(WIRELESS TECH)', 2, 1, 1, 'Fourth', 'First', '2016-2017', 'None', 4, 0),
(54, 'IT 125', 'IT ELECTIVE 4(JAVA PROGRAMMING)', 2, 1, 1, 'Fourth', 'First', '2016-2017', 'None', 4, 0),
(55, 'IT 121', 'MOBILE COMPUTING', 3, 0, 1, 'Fourth', 'First', '2016-2017', 'None', 4, 0),
(56, 'IT 132', 'AUTOCAD', 3, 0, 1, 'Fourth', 'First', '2016-2017', 'None', 4, 0),
(57, 'IT 128', 'IT ELECTIVE 5(ARTIFICIAL INTELLIGENCE)', 2, 1, 1, 'Fourth', 'First', '2016-2017', 'None', 4, 0),
(58, 'IT 129', 'OJT/INTERNSHIP PROGRAM(600 HOURS)', 0, 9, 1, 'Fourth', 'Second', '2016-2017', 'None', 4, 0),
(59, 'FIL 11', 'KOMUNIKASYON SA AKADEMIKONG FILIPINO', 3, 0, 1, 'First', 'First', '2016-2017', 'None', 1, 0),
(60, 'PE 11', 'PHYSICAL FITNESS & SELF TESTING ACTIVITIES', 2, 0, 1, 'First', 'First', '2016-2017', 'None', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbluseraccount`
--

CREATE TABLE IF NOT EXISTS `tbluseraccount` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `Fullname` varchar(255) NOT NULL,
  `User_name` varchar(255) NOT NULL,
  `Pass` varchar(255) NOT NULL,
  `UserType` varchar(255) NOT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbluseraccount`
--

INSERT INTO `tbluseraccount` (`UserId`, `Fullname`, `User_name`, `Pass`, `UserType`) VALUES
(1, 'Janobe', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `tried`
--

CREATE TABLE IF NOT EXISTS `tried` (
  `TREY` int(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tried`
--

INSERT INTO `tried` (`TREY`) VALUES
(1),
(2);
