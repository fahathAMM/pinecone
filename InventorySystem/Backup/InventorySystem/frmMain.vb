﻿Public Class frmMain

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Timer1.Start()


    End Sub


    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        If lbllog.Text = "Log Out" Then
            lbllog.Text = "Log in"
            disableall()
            Me.Hide()
            frmLog.ShowDialog()
        Else
            frmLog.ShowDialog()
        End If

    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lbltime.Text = DateAndTime.Now
    End Sub

    Private Sub btnDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDatabase.Click
        frmCreateDatabase.ShowDialog()
    End Sub

    Private Sub btnItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnItem.Click
        frmItems.ShowDialog()
    End Sub

    Private Sub btnSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSupplier.Click
        frmSupplier.ShowDialog()
    End Sub

    
    Private Sub btnStockin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockin.Click
        frmStockin.ShowDialog()
    End Sub

    Private Sub btnOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOrder.Click
        frmTransaction.ShowDialog()
    End Sub

    Private Sub btnReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReports.Click
        frmor.showdialog()
    End Sub

    Private Sub btnAddUsers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddUsers.Click
        frmusers.ShowDialog()
    End Sub

    Private Sub btnStockOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockOut.Click
        frmStockout.ShowDialog()
    End Sub
End Class
