﻿Public Class frmAddstock
    Dim sidno As Integer
    Dim itemidno As Integer
    Dim datenow As String

    Private Sub frmAddstock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mySelectQuery("SELECT `SIDNO`,`SNAME`  FROM `tblsupplier` WHERE 1")
        fillcombo(cbsuppliername, "SNAME", "SIDNO")

        mySelectQuery("SELECT `ITEMIDNO`,`INAME`  FROM `tblitem` WHERE 1")
        fillcombo(cbitemname, "INAME", "ITEMIDNO")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        datenow = Format(dtpdateentered.Value, "yyyy-MM-dd")
        IsResult = myInsert("INSERT INTO `tblstockin`(`SIDNO`, `ITEMIDNO`, `ITEMCODE`, `SIQUANTITY`, `SIPRICE`, `SIDATE`) VALUES (" & cbsuppliername.SelectedValue & "," & cbitemname.SelectedValue & ",'" & txtitemcode.Text & "'," & txtquantity.Text & "," & txtprice.Text & ",'" & datenow.ToString & "')")

        If IsResult = True Then
            MsgBox("Adding stock successfully!")
            cbsuppliername.Text = Nothing
            cbitemname.Text = Nothing
            txtitemcode.Clear()
            txtquantity.Clear()
            txtprice.Clear()
        End If
    End Sub
End Class