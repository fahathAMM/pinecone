﻿Public Class frmItems

    Public Sub frmItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mySelectQuery("SELECT `ITEMIDNO`, `INAME`, `IDESCRIPTION` FROM `tblitem` WHERE 1")
        LoadData(DataGridView1, "ItemLoad")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frmAddnew.ShowDialog()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ITEMIDNO = DataGridView1.CurrentRow.Cells(1).Value
        IsResult = myDelete("Delete from `tblitem` WHERE `ITEMIDNO` =" & ITEMIDNO & "")
        If IsResult = True Then
            MsgBox("Item Deleted successfully!")
            mySelectQuery("SELECT `ITEMIDNO`, `INAME`, `IDESCRIPTION` FROM `tblitem` WHERE 1")
            LoadData(DataGridView1, "ItemLoad")
        End If
       
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Close()
    End Sub
End Class