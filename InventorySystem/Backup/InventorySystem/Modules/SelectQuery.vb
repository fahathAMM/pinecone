﻿Imports MySql.Data.MySqlClient
Module SelectQuery
    Dim cmd As New MySqlCommand
    Dim dt As New DataTable
    Dim da As New MySqlDataAdapter
    Dim dbReader As MySqlDataReader
    Dim siidno As Integer

    Public DbAdapter As New MySqlDataAdapter
    Public DbDataReader As MySqlDataReader
    Public DbCmd As New MySqlCommand
    Public DtResult As DataTable
    Public Sub mySelectQuery(ByVal sql As String)
        If ConnectionState.Open Then
            con.Close()
        End If
        Try
            con.Open()
            With cmd
                .Connection = con
                .CommandText = sql
            End With

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        con.Close()
        da.Dispose()
    End Sub

    Public Sub CheckResult(ByVal CaseResult As String)
        Dim table As New DataTable
        Try
            da.SelectCommand = cmd
            da.Fill(table)

            If table.Rows.Count > 0 Then

                Select Case CaseResult
                    Case "ShowDB"
                        frmCreateDatabase.txtshowdatabase.Text = table.Rows(0).Item(0)
                    Case "ShowDBcreate"
                        frmDatabase.txtDatabaseName.Text = table.Rows(0).Item(0)
                End Select

            Else

                Select Case CaseResult
                    Case "ShowDB"

                End Select
            End If

        Catch ex As Exception

        End Try

    End Sub
    Public Sub fillcombo(ByVal combo As Object, ByVal member As String, ByVal idparam As String)

        Dim publictable As New DataTable
        Try
            da.SelectCommand = cmd
            da.Fill(publictable)
            With combo
                .datasource = publictable
                .displaymember = member
                .valuemember = idparam
                .selectedindex = -1
            End With

        Catch ex As Exception

        Finally
            da.Dispose()
            con.Close()
        End Try


    End Sub
    
    Public Sub LoadData(ByVal obj As Object, ByVal caseswitch As String)
        If ConnectionState.Open Then
            con.Close()
        End If
        Try

            con.Open()
            dbReader = cmd.ExecuteReader()
            obj.rows.clear()

            Select Case caseswitch
                Case "ItemLoad"
                    Do While dbReader.Read = True
                        obj.Rows.Add(False, dbReader(0), dbReader(1), dbReader(2))
                    Loop
                Case "userload"
                    Do While dbReader.Read = True
                        obj.Rows.Add(False, dbReader("USERNAME"), dbReader("USERTYPE"))
                    Loop
                Case "SupplierLoad"
                    Do While dbReader.Read = True
                        obj.Rows.Add(False, dbReader(0), dbReader(1), dbReader(2), dbReader(3))
                    Loop
                Case "TableDetails"
                    Do While dbReader.Read = True
                        obj.Rows.Add(dbReader(0), dbReader(1), dbReader(2), dbReader(3), dbReader(4), dbReader(5))
                    Loop
                Case "StockInLoad"
                    Do While dbReader.Read = True
                        obj.Rows.Add(False, dbReader(0), dbReader(1), dbReader(2), dbReader(3), dbReader(4), dbReader(5), dbReader(6))
                    Loop
                Case "SOLOAD"
                    Do While dbReader.Read = True
                        obj.Rows.Add(False, dbReader(0), dbReader(1), dbReader(2), dbReader(3), dbReader(4))
                    Loop
                Case "orload"
                    Do While dbReader.Read = True
                        obj.Rows.Add(False, dbReader(0), dbReader(1), dbReader(2), dbReader(3), dbReader(4), dbReader(5), dbReader(6))
                    Loop
            End Select

        Catch ex As Exception

        End Try
        con.Close()
    End Sub
    Public Function getsvalue()
        Dim re = Nothing

        con.Open()
        dbReader = cmd.ExecuteReader

        While dbReader.Read
            re = dbReader(0)
        End While

        con.Close()
        Return re
    End Function
End Module
