﻿Imports MySql.Data.MySqlClient
Module Public_Variables
    Public con As MySqlConnection = myconn()
    Public IsResult As Boolean
    Public datenow As String
    Public ITEMIDNO As Integer
    Public Username As String = ""
    Public UserId As String = ""
    Public UserPassword As String = ""
    Public userType As String = ""
    Public itemcode As String = ""
    Public itemqty As Integer
    Public itemnap As String


    Public Function SetDbConnectionString()

        Dim DbSetup As String = "server=" & frmDatabase.txtServer.Text & "; user id=" & frmDatabase.txtUser.Text & "; password=" & frmDatabase.txtPassword.Text & "; database=" & frmDatabase.txtDatabaseName.Text & ";"

        Return New MySqlConnection(DbSetup)
    End Function

    Public Sub SetConnection()
        If con.State = ConnectionState.Open Then con.Close()

        con.Open()
    End Sub

    Public Function GetSingleValue(ByVal Query As String) As String
        Dim Result As String = New String("")

        Try
            SetConnection()
            DbCmd = New MySqlCommand(Query, con)

            DbDataReader = DbCmd.ExecuteReader
            While DbDataReader.Read
                If Not IsDBNull(DbDataReader(0)) Then
                    Result = DbDataReader(0)
                End If
            End While
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Return Result
    End Function

    Public Sub SetAutoCompletion(ByVal SqlQuery As String, ByVal TargetTextBox As TextBox)
        Dim Result As New AutoCompleteStringCollection

        SetConnection()

        Try
            DbCmd = New MySqlCommand(SqlQuery, con)

            DbDataReader = DbCmd.ExecuteReader
            While DbDataReader.Read()
                Result.Add(DbDataReader.GetValue(0))
            End While
            DbDataReader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        TargetTextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        TargetTextBox.AutoCompleteSource = AutoCompleteSource.CustomSource
        TargetTextBox.AutoCompleteCustomSource = Result
    End Sub
    Public Sub supplier()
        frmMain.btnStockin.Enabled = True
    End Sub
    Public Sub company()
        With frmMain
            .btnItem.Enabled = True
            .btnOrder.Enabled = True
            .btnReports.Enabled = True
            .btnStockin.Enabled = True
            .btnStockOut.Enabled = True
            .btnSupplier.Enabled = True
        End With

    End Sub
    Public Sub admin()
        With frmMain
            .btnAddUsers.Enabled = True
            .btnDatabase.Enabled = True
            .btnItem.Enabled = True
            .btnOrder.Enabled = True
            .btnReports.Enabled = True
            .btnStockin.Enabled = True
            .btnStockOut.Enabled = True
            .btnSupplier.Enabled = True

        End With

    End Sub
    Public Sub disableall()
        With frmMain
            .btnAddUsers.Enabled = False
            .btnDatabase.Enabled = False
            .btnItem.Enabled = False
            .btnOrder.Enabled = False
            .btnReports.Enabled = False
            .btnStockin.Enabled = False
            .btnStockOut.Enabled = False
            .btnSupplier.Enabled = False
        End With
    End Sub

End Module
