﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCreateTable
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnalter = New System.Windows.Forms.Button
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.n_Field = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.n_Type = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.n_Values = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cbtable = New System.Windows.Forms.ComboBox
        Me.btncreate = New System.Windows.Forms.Button
        Me.txtlengthvalue = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cbtype = New System.Windows.Forms.ComboBox
        Me.txtfieldname = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txttablename = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtdatabase = New System.Windows.Forms.TextBox
        Me.btnShowdatabase = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnalter
        '
        Me.btnalter.Location = New System.Drawing.Point(719, 340)
        Me.btnalter.Name = "btnalter"
        Me.btnalter.Size = New System.Drawing.Size(141, 33)
        Me.btnalter.TabIndex = 26
        Me.btnalter.Text = "Alter"
        Me.btnalter.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.n_Field, Me.n_Type, Me.n_Values})
        Me.DataGridView1.Location = New System.Drawing.Point(262, 51)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(598, 283)
        Me.DataGridView1.TabIndex = 25
        '
        'n_Field
        '
        Me.n_Field.HeaderText = "Field Name"
        Me.n_Field.Name = "n_Field"
        '
        'n_Type
        '
        Me.n_Type.HeaderText = "Type"
        Me.n_Type.Items.AddRange(New Object() {"INT", "VARCHAR", "TEXT", "DATE", "DOUBLE"})
        Me.n_Type.Name = "n_Type"
        Me.n_Type.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.n_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'n_Values
        '
        Me.n_Values.HeaderText = "Length Values"
        Me.n_Values.Name = "n_Values"
        '
        'cbtable
        '
        Me.cbtable.FormattingEnabled = True
        Me.cbtable.Location = New System.Drawing.Point(581, 13)
        Me.cbtable.Name = "cbtable"
        Me.cbtable.Size = New System.Drawing.Size(279, 33)
        Me.cbtable.TabIndex = 24
        '
        'btncreate
        '
        Me.btncreate.Location = New System.Drawing.Point(12, 346)
        Me.btncreate.Name = "btncreate"
        Me.btncreate.Size = New System.Drawing.Size(141, 33)
        Me.btncreate.TabIndex = 23
        Me.btncreate.Text = "Create"
        Me.btncreate.UseVisualStyleBackColor = True
        '
        'txtlengthvalue
        '
        Me.txtlengthvalue.Location = New System.Drawing.Point(12, 301)
        Me.txtlengthvalue.Name = "txtlengthvalue"
        Me.txtlengthvalue.Size = New System.Drawing.Size(244, 33)
        Me.txtlengthvalue.TabIndex = 22
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 277)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(126, 25)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Length Value:"
        '
        'cbtype
        '
        Me.cbtype.FormattingEnabled = True
        Me.cbtype.Items.AddRange(New Object() {"INT", "VARCHAR", "TEXT", "DATE", "DOUBLE"})
        Me.cbtype.Location = New System.Drawing.Point(12, 238)
        Me.cbtype.Name = "cbtype"
        Me.cbtype.Size = New System.Drawing.Size(244, 33)
        Me.cbtype.TabIndex = 20
        '
        'txtfieldname
        '
        Me.txtfieldname.Location = New System.Drawing.Point(12, 174)
        Me.txtfieldname.Name = "txtfieldname"
        Me.txtfieldname.Size = New System.Drawing.Size(244, 33)
        Me.txtfieldname.TabIndex = 19
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 150)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(111, 25)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Field Name:"
        '
        'txttablename
        '
        Me.txttablename.Location = New System.Drawing.Point(12, 111)
        Me.txttablename.Name = "txttablename"
        Me.txttablename.Size = New System.Drawing.Size(244, 33)
        Me.txttablename.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 87)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 25)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Table Name:"
        '
        'txtdatabase
        '
        Me.txtdatabase.Location = New System.Drawing.Point(12, 51)
        Me.txtdatabase.Name = "txtdatabase"
        Me.txtdatabase.Size = New System.Drawing.Size(244, 33)
        Me.txtdatabase.TabIndex = 15
        '
        'btnShowdatabase
        '
        Me.btnShowdatabase.Location = New System.Drawing.Point(12, 12)
        Me.btnShowdatabase.Name = "btnShowdatabase"
        Me.btnShowdatabase.Size = New System.Drawing.Size(154, 33)
        Me.btnShowdatabase.TabIndex = 14
        Me.btnShowdatabase.Text = "Show Database"
        Me.btnShowdatabase.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 210)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 25)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Type:"
        '
        'frmCreateTable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(872, 395)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnalter)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.cbtable)
        Me.Controls.Add(Me.btncreate)
        Me.Controls.Add(Me.txtlengthvalue)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cbtype)
        Me.Controls.Add(Me.txtfieldname)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txttablename)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtdatabase)
        Me.Controls.Add(Me.btnShowdatabase)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCreateTable"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Table"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnalter As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents n_Field As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents n_Type As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents n_Values As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbtable As System.Windows.Forms.ComboBox
    Friend WithEvents btncreate As System.Windows.Forms.Button
    Friend WithEvents txtlengthvalue As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbtype As System.Windows.Forms.ComboBox
    Friend WithEvents txtfieldname As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txttablename As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtdatabase As System.Windows.Forms.TextBox
    Friend WithEvents btnShowdatabase As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
