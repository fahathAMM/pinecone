﻿Public Class frmCreateDatabase

    Private Sub frmCreateDatabase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mySelectQuery("SELECT DATABASE()")
        CheckResult("ShowDB")

        mySelectQuery("SHOW Tables")
        fillcombo(cbshowtables, "Tables_in_" & txtshowdatabase.Text & "", "Tables_in_" & txtshowdatabase.Text & "")
       
       
    End Sub

    Private Sub btnCreateTAble_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateTAble.Click
        myInsert("CREATE DATABASE " & txtcreatedatabase.Text & "")
        MsgBox("Database created!")
        txtcreatedatabase.Clear()
    End Sub

    Private Sub btnconnecting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconnecting.Click
        frmDatabase.ShowDialog()
    End Sub

    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        frmCreateTable.ShowDialog()
    End Sub

    Private Sub btnDescribetables_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Private Sub cbshowtables_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbshowtables.TextChanged
        mySelectQuery("DESCRIBE " & cbshowtables.Text & "")
        LoadData(DataGridView1, "TableDetails")
        If cbshowtables.Text = Nothing Then
            DataGridView1.Rows.Clear()
        End If

    End Sub
End Class