﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label7 = New System.Windows.Forms.Label
        Me.btnSupplier = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.btnReports = New System.Windows.Forms.Button
        Me.btnAddUsers = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnLogin = New System.Windows.Forms.Button
        Me.lbllog = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.btnStockOut = New System.Windows.Forms.Button
        Me.btnStockin = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnItem = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnDatabase = New System.Windows.Forms.Button
        Me.btnOrder = New System.Windows.Forms.Button
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.Splitter2 = New System.Windows.Forms.Splitter
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lblUsertype = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lbltime = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.btnSupplier)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.btnReports)
        Me.Panel1.Controls.Add(Me.btnAddUsers)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.btnLogin)
        Me.Panel1.Controls.Add(Me.lbllog)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.btnStockOut)
        Me.Panel1.Controls.Add(Me.btnStockin)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.btnItem)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btnDatabase)
        Me.Panel1.Controls.Add(Me.btnOrder)
        Me.Panel1.Controls.Add(Me.Splitter1)
        Me.Panel1.Controls.Add(Me.Splitter2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1069, 108)
        Me.Panel1.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label7.Location = New System.Drawing.Point(542, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 21)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "SUPPLIER"
        '
        'btnSupplier
        '
        Me.btnSupplier.BackgroundImage = CType(resources.GetObject("btnSupplier.BackgroundImage"), System.Drawing.Image)
        Me.btnSupplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnSupplier.Enabled = False
        Me.btnSupplier.Location = New System.Drawing.Point(542, 12)
        Me.btnSupplier.Name = "btnSupplier"
        Me.btnSupplier.Size = New System.Drawing.Size(76, 59)
        Me.btnSupplier.TabIndex = 20
        Me.btnSupplier.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label9.Location = New System.Drawing.Point(437, 74)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 21)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "REPORTS"
        '
        'btnReports
        '
        Me.btnReports.BackgroundImage = CType(resources.GetObject("btnReports.BackgroundImage"), System.Drawing.Image)
        Me.btnReports.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnReports.Enabled = False
        Me.btnReports.Location = New System.Drawing.Point(435, 12)
        Me.btnReports.Name = "btnReports"
        Me.btnReports.Size = New System.Drawing.Size(77, 59)
        Me.btnReports.TabIndex = 18
        Me.btnReports.UseVisualStyleBackColor = True
        '
        'btnAddUsers
        '
        Me.btnAddUsers.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddUsers.Enabled = False
        Me.btnAddUsers.Image = CType(resources.GetObject("btnAddUsers.Image"), System.Drawing.Image)
        Me.btnAddUsers.Location = New System.Drawing.Point(768, 12)
        Me.btnAddUsers.Name = "btnAddUsers"
        Me.btnAddUsers.Size = New System.Drawing.Size(77, 59)
        Me.btnAddUsers.TabIndex = 16
        Me.btnAddUsers.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label8.Location = New System.Drawing.Point(777, 74)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 21)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "USERS"
        '
        'btnLogin
        '
        Me.btnLogin.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogin.Image = CType(resources.GetObject("btnLogin.Image"), System.Drawing.Image)
        Me.btnLogin.Location = New System.Drawing.Point(935, 12)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(78, 59)
        Me.btnLogin.TabIndex = 6
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'lbllog
        '
        Me.lbllog.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbllog.AutoSize = True
        Me.lbllog.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lbllog.Location = New System.Drawing.Point(942, 74)
        Me.lbllog.Name = "lbllog"
        Me.lbllog.Size = New System.Drawing.Size(60, 21)
        Me.lbllog.TabIndex = 8
        Me.lbllog.Text = "LOG IN"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label6.Location = New System.Drawing.Point(319, 74)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(92, 21)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "STOCK OUT"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label5.Location = New System.Drawing.Point(218, 74)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 21)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "STOCK IN"
        '
        'btnStockOut
        '
        Me.btnStockOut.BackgroundImage = CType(resources.GetObject("btnStockOut.BackgroundImage"), System.Drawing.Image)
        Me.btnStockOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnStockOut.Enabled = False
        Me.btnStockOut.Location = New System.Drawing.Point(326, 12)
        Me.btnStockOut.Name = "btnStockOut"
        Me.btnStockOut.Size = New System.Drawing.Size(77, 59)
        Me.btnStockOut.TabIndex = 10
        Me.btnStockOut.UseVisualStyleBackColor = True
        '
        'btnStockin
        '
        Me.btnStockin.BackgroundImage = CType(resources.GetObject("btnStockin.BackgroundImage"), System.Drawing.Image)
        Me.btnStockin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnStockin.Enabled = False
        Me.btnStockin.Image = CType(resources.GetObject("btnStockin.Image"), System.Drawing.Image)
        Me.btnStockin.Location = New System.Drawing.Point(219, 12)
        Me.btnStockin.Name = "btnStockin"
        Me.btnStockin.Size = New System.Drawing.Size(77, 59)
        Me.btnStockin.TabIndex = 9
        Me.btnStockin.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(663, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 21)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "DATABASE"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label2.Location = New System.Drawing.Point(129, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 21)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "ITEM"
        '
        'btnItem
        '
        Me.btnItem.Enabled = False
        Me.btnItem.Image = CType(resources.GetObject("btnItem.Image"), System.Drawing.Image)
        Me.btnItem.Location = New System.Drawing.Point(113, 12)
        Me.btnItem.Name = "btnItem"
        Me.btnItem.Size = New System.Drawing.Size(77, 59)
        Me.btnItem.TabIndex = 4
        Me.btnItem.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.Location = New System.Drawing.Point(20, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 21)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "ORDER"
        '
        'btnDatabase
        '
        Me.btnDatabase.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDatabase.Enabled = False
        Me.btnDatabase.Image = CType(resources.GetObject("btnDatabase.Image"), System.Drawing.Image)
        Me.btnDatabase.Location = New System.Drawing.Point(669, 12)
        Me.btnDatabase.Name = "btnDatabase"
        Me.btnDatabase.Size = New System.Drawing.Size(77, 59)
        Me.btnDatabase.TabIndex = 1
        Me.btnDatabase.UseVisualStyleBackColor = True
        '
        'btnOrder
        '
        Me.btnOrder.Enabled = False
        Me.btnOrder.Image = CType(resources.GetObject("btnOrder.Image"), System.Drawing.Image)
        Me.btnOrder.Location = New System.Drawing.Point(12, 12)
        Me.btnOrder.Name = "btnOrder"
        Me.btnOrder.Size = New System.Drawing.Size(76, 59)
        Me.btnOrder.TabIndex = 0
        Me.btnOrder.UseVisualStyleBackColor = True
        '
        'Splitter1
        '
        Me.Splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter1.Location = New System.Drawing.Point(0, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(647, 108)
        Me.Splitter1.TabIndex = 2
        Me.Splitter1.TabStop = False
        '
        'Splitter2
        '
        Me.Splitter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Splitter2.Location = New System.Drawing.Point(870, 0)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(199, 108)
        Me.Splitter2.TabIndex = 15
        Me.Splitter2.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel3.Controls.Add(Me.lblUsertype)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.lbltime)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 513)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1069, 24)
        Me.Panel3.TabIndex = 2
        '
        'lblUsertype
        '
        Me.lblUsertype.AutoSize = True
        Me.lblUsertype.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblUsertype.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.lblUsertype.Location = New System.Drawing.Point(216, 0)
        Me.lblUsertype.Name = "lblUsertype"
        Me.lblUsertype.Size = New System.Drawing.Size(86, 21)
        Me.lblUsertype.TabIndex = 5
        Me.lblUsertype.Text = "USER TYPE"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label10.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.Label10.Location = New System.Drawing.Point(0, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(216, 21)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "YOU'RE NOW LOGGED IN AS:"
        '
        'lbltime
        '
        Me.lbltime.AutoSize = True
        Me.lbltime.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbltime.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.lbltime.Location = New System.Drawing.Point(1025, 0)
        Me.lbltime.Name = "lbltime"
        Me.lbltime.Size = New System.Drawing.Size(44, 21)
        Me.lbltime.TabIndex = 3
        Me.lbltime.Text = "TIME"
        '
        'Timer1
        '
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1069, 537)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel3)
        Me.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Main Form"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents btnDatabase As System.Windows.Forms.Button
    Friend WithEvents btnOrder As System.Windows.Forms.Button
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnItem As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lbllog As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnStockOut As System.Windows.Forms.Button
    Friend WithEvents btnStockin As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnReports As System.Windows.Forms.Button
    Friend WithEvents btnAddUsers As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents lbltime As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblUsertype As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnSupplier As System.Windows.Forms.Button

End Class
