﻿Imports MySql.Data.MySqlClient
Module CRUDQuery
    Dim result As Integer
    Dim cmd As New MySqlCommand
    Public Function myInsert(ByVal sql As String) As Boolean
        If ConnectionState.Open Then
            con.Close()
        End If
        Try
            con.Open()
            With cmd
                .Connection = con
                .CommandText = sql

                result = cmd.ExecuteNonQuery
                If result = 0 Then
                    Return False
                Else
                    Return True
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
        con.Close()

    End Function
    Public Function myDelete(ByVal sql As String) As Boolean
        If ConnectionState.Open Then
            con.Close()
        End If
        Try
            con.Open()
            With cmd
                .Connection = con
                .CommandText = sql

                result = cmd.ExecuteNonQuery
                If result = 0 Then
                    Return False
                Else
                    Return True
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
        con.Close()

    End Function
    Public Function myUpdate(ByVal sql As String) As Boolean
        If ConnectionState.Open Then
            con.Close()
        End If
        Try
            con.Open()
            With cmd
                .Connection = con
                .CommandText = sql

                result = cmd.ExecuteNonQuery
                If result = 0 Then
                    Return False
                Else
                    Return True
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
        con.Close()

    End Function
End Module
