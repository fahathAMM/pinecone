﻿Public Class frmStockin

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frmAddstock.ShowDialog()
    End Sub

    Private Sub frmStockin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mySelectQuery("SELECT  si.`SIIDNO`,s.`SNAME`, i.`INAME` ,si.`ITEMCODE`,si.`SIQUANTITY`,si.`SIPRICE`,si.`SIDATE`FROM `tblitem` i,`tblstockin` si ,`tblsupplier` s WHERE i.`ITEMIDNO` = si.`ITEMIDNO` AND s.`SIDNO` = si.`SIDNO`")

        LoadData(DataGridView1, "StockInLoad")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Close()
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If Button1.Visible = False Then
            frmTransaction.txtitemname.Text = DataGridView1.CurrentRow.Cells(3).Value & " | " & DataGridView1.CurrentRow.Cells(2).Value & " | " & DataGridView1.CurrentRow.Cells(4).Value
            ITEMIDNO = DataGridView1.CurrentRow.Cells(1).Value
            frmTransaction.txtindividualprice.Text = DataGridView1.CurrentRow.Cells(6).Value
            itemnap = DataGridView1.CurrentRow.Cells(3).Value & " | " & DataGridView1.CurrentRow.Cells(2).Value
            itemcode = DataGridView1.CurrentRow.Cells(4).Value
            itemqty = DataGridView1.CurrentRow.Cells(5).Value

            Button1.Visible = True
            Close()
        Else

        End If
    End Sub
End Class