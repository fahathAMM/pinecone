﻿Imports MySql.Data.MySqlClient
Public Class frmLog

    Private Sub btnEnter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnter.Click
        Username = txtUsername.Text
        If txtPassword.Text = "" Or txtUsername.Text = "" Then
            MessageBox.Show("Please Fill in Empty TextBox!")
        Else


            Try

                Dim query As String
                query = "select usertype from tblusers where Username= ‘" & txtUsername.Text & "‘ and Password = ‘" & txtPassword.Text & "‘ "
                DbCmd = New MySqlCommand(query, con)
                DbDataReader = DbCmd.ExecuteReader

                Dim count As Integer
                count = 0
                While DbDataReader.Read
                    count = count + 1
                End While
               
                If count = 1 Then

                    Dim usertype = DbDataReader.GetString("usertype")
                  
                    
                    If usertype = "supplier" Then
                        supplier()
                        frmMain.lbllog.Text = "Log Out"
                        MsgBox("Welcome supplier")
                        frmMain.lblUsertype.Text = txtUsername.Text & " as " & usertype
                        For j = 0 To 500


                        Next
                        txtPassword.Clear()
                        txtUsername.Clear()
                        Hide()
                        If frmMain.Visible = False Then
                            frmMain.Visible = True
                        Else
                            frmMain.ShowDialog()
                        End If

                        If ConnectionState.Open Then
                            con.Close()
                            con.Dispose()
                        End If

                    ElseIf usertype = "company" Then


                        For j = 0 To 500

                        Next
                        company()
                        frmMain.lbllog.Text = "Log Out"
                        MsgBox("Welcome company")
                        frmMain.lblUsertype.Text = txtUsername.Text & " as " & usertype
                        txtPassword.Clear()
                        txtUsername.Clear()
                        Hide()
                        If frmMain.Visible = False Then
                            frmMain.Visible = True
                        Else
                            frmMain.ShowDialog()
                        End If

                    ElseIf usertype = "admin" Then


                        For j = 0 To 500

                        Next
                        admin()
                        frmMain.lbllog.Text = "Log Out"
                        MsgBox("Welcome admin")
                        frmMain.lblUsertype.Text = txtUsername.Text & " as " & usertype
                        txtPassword.Clear()
                        txtUsername.Clear()
                        Hide()
                        If frmMain.Visible = False Then
                            frmMain.Visible = True
                        Else
                            frmMain.ShowDialog()
                        End If

                    End If
                Else
                    MsgBox("User does not exist!")
                    txtUsername.Text = ""
                    txtPassword.Text = ""
                    If ConnectionState.Open Then
                        con.Close()
                        con.Dispose()
                    End If
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally

                txtUsername.Text = ""
                txtPassword.Text = ""
            End Try
           
        End If
       
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub frmLog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SetAutoCompletion("SELECT DISTINCT username FROM tblusers", txtUsername)

    End Sub
End Class