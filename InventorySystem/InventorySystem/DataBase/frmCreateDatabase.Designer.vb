﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCreateDatabase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnnew = New System.Windows.Forms.Button
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Field = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Type = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Null = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtcreatedatabase = New System.Windows.Forms.TextBox
        Me.cbshowtables = New System.Windows.Forms.ComboBox
        Me.txtshowdatabase = New System.Windows.Forms.TextBox
        Me.btnCreateTAble = New System.Windows.Forms.Button
        Me.btnconnecting = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnnew
        '
        Me.btnnew.Location = New System.Drawing.Point(769, 374)
        Me.btnnew.Name = "btnnew"
        Me.btnnew.Size = New System.Drawing.Size(108, 37)
        Me.btnnew.TabIndex = 22
        Me.btnnew.Text = "New"
        Me.btnnew.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Field, Me.Type, Me.Null, Me.Column4, Me.Column5, Me.Column6})
        Me.DataGridView1.Location = New System.Drawing.Point(12, 110)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(865, 256)
        Me.DataGridView1.TabIndex = 21
        '
        'Field
        '
        Me.Field.HeaderText = "Field Name"
        Me.Field.Name = "Field"
        Me.Field.ReadOnly = True
        '
        'Type
        '
        Me.Type.HeaderText = "Type"
        Me.Type.Name = "Type"
        Me.Type.ReadOnly = True
        '
        'Null
        '
        Me.Null.HeaderText = "Null"
        Me.Null.Name = "Null"
        Me.Null.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Key"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Default"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "Extra"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'txtcreatedatabase
        '
        Me.txtcreatedatabase.Location = New System.Drawing.Point(519, 13)
        Me.txtcreatedatabase.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtcreatedatabase.Name = "txtcreatedatabase"
        Me.txtcreatedatabase.Size = New System.Drawing.Size(258, 33)
        Me.txtcreatedatabase.TabIndex = 20
        '
        'cbshowtables
        '
        Me.cbshowtables.FormattingEnabled = True
        Me.cbshowtables.Location = New System.Drawing.Point(12, 55)
        Me.cbshowtables.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cbshowtables.Name = "cbshowtables"
        Me.cbshowtables.Size = New System.Drawing.Size(499, 33)
        Me.cbshowtables.TabIndex = 19
        '
        'txtshowdatabase
        '
        Me.txtshowdatabase.Location = New System.Drawing.Point(237, 14)
        Me.txtshowdatabase.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtshowdatabase.Name = "txtshowdatabase"
        Me.txtshowdatabase.Size = New System.Drawing.Size(274, 33)
        Me.txtshowdatabase.TabIndex = 18
        '
        'btnCreateTAble
        '
        Me.btnCreateTAble.Location = New System.Drawing.Point(785, 11)
        Me.btnCreateTAble.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnCreateTAble.Name = "btnCreateTAble"
        Me.btnCreateTAble.Size = New System.Drawing.Size(92, 37)
        Me.btnCreateTAble.TabIndex = 17
        Me.btnCreateTAble.Text = "Create Database"
        Me.btnCreateTAble.UseVisualStyleBackColor = True
        '
        'btnconnecting
        '
        Me.btnconnecting.Location = New System.Drawing.Point(519, 55)
        Me.btnconnecting.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnconnecting.Name = "btnconnecting"
        Me.btnconnecting.Size = New System.Drawing.Size(358, 37)
        Me.btnconnecting.TabIndex = 16
        Me.btnconnecting.Text = "Connected Database"
        Me.btnconnecting.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(218, 25)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Current Database Name:"
        '
        'frmCreateDatabase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(893, 426)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnnew)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.txtcreatedatabase)
        Me.Controls.Add(Me.cbshowtables)
        Me.Controls.Add(Me.txtshowdatabase)
        Me.Controls.Add(Me.btnCreateTAble)
        Me.Controls.Add(Me.btnconnecting)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCreateDatabase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Database"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnnew As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Field As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Null As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtcreatedatabase As System.Windows.Forms.TextBox
    Friend WithEvents cbshowtables As System.Windows.Forms.ComboBox
    Friend WithEvents txtshowdatabase As System.Windows.Forms.TextBox
    Friend WithEvents btnCreateTAble As System.Windows.Forms.Button
    Friend WithEvents btnconnecting As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
