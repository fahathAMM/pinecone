﻿Public Class frmCreateTable


    Private Sub frmCreateTable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtdatabase.Text = frmCreateDatabase.txtshowdatabase.Text
        mySelectQuery("SHOW TABLES")

        fillcombo(cbtable, "Tables_in_" & frmDatabase.txtDatabaseName.Text & "", "Tables_in_" & frmDatabase.txtDatabaseName.Text & "")
    End Sub

    Private Sub btncreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncreate.Click
        myInsert("CREATE TABLE " & txtdatabase.Text & "." & txttablename.Text & " (" & txtfieldname.Text & " INT (" & txtlengthvalue.Text & ") NOT NULL AUTO_INCREMENT PRIMARY KEY) ENGINE = INNODB;")
    End Sub

    Private Sub btnalter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnalter.Click
        Dim field, type, values As String

        For i As Integer = 0 To DataGridView1.Rows.Count - 2
            field = DataGridView1.Rows(i).Cells("n_Field").Value
            type = DataGridView1.Rows(i).Cells("n_Type").Value
            values = DataGridView1.Rows(i).Cells("n_Values").Value

            myInsert("ALTER TABLE " & cbtable.Text & " ADD " & field & " " & type & "( " & values & ") NOT NULL")

        Next
        DataGridView1.Rows.Clear()
    End Sub

End Class