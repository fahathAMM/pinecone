﻿Public Class frmTransaction
    Dim firstnumber As Integer
    Dim secondnumber As Integer
    Dim total As Integer = 0
   
    Private Sub frmTransaction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mySelectQuery("SELECT  si.`SIIDNO`,concat(i.`INAME` ,' | ',si.`ITEMCODE`,' | ',s.`SNAME`) as ncs ,si.`SIQUANTITY`,si.`SIPRICE`,si.`SIDATE`FROM `tblitem` i,`tblstockin` si ,`tblsupplier` s WHERE i.`ITEMIDNO` = si.`ITEMIDNO` AND s.`SIDNO` = si.`SIDNO`")
        fillcombo(txtitemname, "ncs", "si.SIIDNO")
       
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frmStockin.Button1.Visible = False
        frmStockin.ShowDialog()

    End Sub

    Public Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If Val(itemqty) = 0 Or Val(itemqty) - Val(txtquantity.Text) < 0 Then
            MsgBox("Item out of stock, Please contact your supplier.")
        Else

            IsResult = myUpdate("UPDATE `tblstockin` SET `SIQUANTITY`=" & itemqty - Val(txtquantity.Text) & " WHERE `ITEMCODE`='" & itemcode & "'")
            firstnumber = Val(txtindividualprice.Text) * Val(txtquantity.Text)

            If IsResult = True Then
                IsResult = myInsert("INSERT INTO `tblstockout`(`EINCHARGE`, `ITEMID`, `QUANTITY`, `TOTALPRICE`) VALUES ('" & Username & "'," & ITEMIDNO & "," & txtquantity.Text & "," & firstnumber & ")")
            End If
            If Val(txttotalprice.Text) = 0 Then
                txttotalprice.Text = Val(firstnumber)

                secondnumber = Val(txttotalprice.Text)
            Else
                secondnumber = Val(firstnumber) + Val(txttotalprice.Text)
                txttotalprice.Text = Val(secondnumber)
            End If

            If IsResult = True Then

                DataGridView1.Rows.Add(itemnap, itemcode, firstnumber)
                itemqty = Nothing
                txtquantity.Clear()
                txtitemname.Clear()
                txtindividualprice.Clear()

            End If

        End If

        

    End Sub

    Private Sub txtreceivedamount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtreceivedamount.TextChanged
        If Val(txttotalprice.Text) >= Val(txtreceivedamount.Text) Then
            txtchange.Text = 0
        Else
            txtchange.Text = Format(Val(txtreceivedamount.Text) - Val(txttotalprice.Text), "0.00")
        End If
    End Sub

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        If txtcustomername.Text = Nothing Then
            MsgBox("Please Identify customer's Fullname!")
        Else
            datenow = Format(DateTimePicker1.Value, "yyyy-MM-dd")
            myInsert("INSERT INTO `tblofficialreceipt`(`EINCHARGE`, `CNAME`, `RAMOUNT`, `TPAYMENT`, `CHANGED`, `DOT`) VALUES ('" & Username & "','" & txtcustomername.Text & "'," & txtreceivedamount.Text & "," & txttotalprice.Text & "," & txtchange.Text & ",'" & datenow.ToString & "')")
            MsgBox("Transaction Recorder!")
            txttotalprice.Clear()
            txtcustomername.Clear()
            txtreceivedamount.Clear()
            txtchange.Clear()
            frmStockin.Button1.Visible = True
            DataGridView1.Rows.Clear()
        End If
       
    End Sub
End Class