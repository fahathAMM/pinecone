-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2017 at 08:34 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbinventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblitem`
--

CREATE TABLE `tblitem` (
  `ITEMIDNO` int(12) NOT NULL,
  `INAME` varchar(50) NOT NULL,
  `IDESCRIPTION` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblitem`
--

INSERT INTO `tblitem` (`ITEMIDNO`, `INAME`, `IDESCRIPTION`) VALUES
(4, 'Mouse', 'Cabled Wire'),
(5, 'Keyboard', 'Cabled Wire'),
(6, 'Monitor', 'Flat paryus sa iya'),
(7, 'HeadSet', 'Cabled'),
(8, 'HEADSET', 'wireless');

-- --------------------------------------------------------

--
-- Table structure for table `tblofficialreceipt`
--

CREATE TABLE `tblofficialreceipt` (
  `ORNO` int(11) NOT NULL,
  `EINCHARGE` varchar(50) NOT NULL,
  `CNAME` varchar(50) NOT NULL,
  `RAMOUNT` int(255) NOT NULL,
  `TPAYMENT` int(255) NOT NULL,
  `CHANGED` int(255) NOT NULL,
  `DOT` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblofficialreceipt`
--

INSERT INTO `tblofficialreceipt` (`ORNO`, `EINCHARGE`, `CNAME`, `RAMOUNT`, `TPAYMENT`, `CHANGED`, `DOT`) VALUES
(19, 'christianV.', 'asd', 4200, 4160, 40, '2017-07-23');

-- --------------------------------------------------------

--
-- Table structure for table `tblstockin`
--

CREATE TABLE `tblstockin` (
  `SIIDNO` int(11) NOT NULL,
  `SIDNO` int(11) NOT NULL,
  `ITEMIDNO` int(11) NOT NULL,
  `ITEMCODE` varchar(50) NOT NULL,
  `SIQUANTITY` int(255) NOT NULL,
  `SIPRICE` int(255) NOT NULL,
  `SIDATE` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstockin`
--

INSERT INTO `tblstockin` (`SIIDNO`, `SIDNO`, `ITEMIDNO`, `ITEMCODE`, `SIQUANTITY`, `SIPRICE`, `SIDATE`) VALUES
(10, 4, 5, 'MY-6969', 45, 567, '2017-07-26'),
(11, 6, 4, 'GI-4656', 50, 456, '2017-07-26'),
(12, 5, 7, 'hy-2626', 230, 356, '2017-07-26');

-- --------------------------------------------------------

--
-- Table structure for table `tblstockout`
--

CREATE TABLE `tblstockout` (
  `SOIDNO` int(11) NOT NULL,
  `EINCHARGE` varchar(50) NOT NULL,
  `ITEMIDNO` int(11) NOT NULL,
  `QUANTITY` int(150) NOT NULL,
  `TOTALPRICE` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstockout`
--

INSERT INTO `tblstockout` (`SOIDNO`, `EINCHARGE`, `ITEMIDNO`, `QUANTITY`, `TOTALPRICE`) VALUES
(30, 'christianV.', 12, 4, 1424),
(31, 'christianV.', 11, 6, 2736),
(32, 'es', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblstocks`
--

CREATE TABLE `tblstocks` (
  `SIIDNO` int(11) NOT NULL,
  `SIDNO` int(11) NOT NULL,
  `ITEMIDNO` int(11) NOT NULL,
  `ITEMCODE` varchar(50) NOT NULL,
  `SIQUANTITY` int(255) NOT NULL,
  `SIPRICE` int(255) NOT NULL,
  `SIDATE` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstocks`
--

INSERT INTO `tblstocks` (`SIIDNO`, `SIDNO`, `ITEMIDNO`, `ITEMCODE`, `SIQUANTITY`, `SIPRICE`, `SIDATE`) VALUES
(2, 4, 5, 'MY-6969', 45, 567, '2017-07-26'),
(3, 6, 4, 'GI-4656', 56, 456, '2017-07-26'),
(4, 5, 7, 'hy-2626', 234, 356, '2017-07-26');

-- --------------------------------------------------------

--
-- Table structure for table `tblsupplier`
--

CREATE TABLE `tblsupplier` (
  `SIDNO` int(11) NOT NULL,
  `SNAME` varchar(50) NOT NULL,
  `SCONTACTNUMBER` int(11) NOT NULL,
  `SADDRESS` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsupplier`
--

INSERT INTO `tblsupplier` (`SIDNO`, `SNAME`, `SCONTACTNUMBER`, `SADDRESS`) VALUES
(4, 'A4TECH', 638535636, 'Kabankalan Guanzon Street'),
(5, 'Acer', 672334244, 'Bangga Kalog Sitio Teruhay'),
(6, 'GADGET WORKS', 737374535, 'nakakapagpabagabag');

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `USERIDNO` int(11) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `USERTYPE` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`USERIDNO`, `USERNAME`, `PASSWORD`, `USERTYPE`) VALUES
(1, 'Aba', 'company', 'company'),
(2, 'Bea', 'supplier', 'supplier'),
(3, 'christianV.', 'admin', 'admin'),
(5, 'meow', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblitem`
--
ALTER TABLE `tblitem`
  ADD PRIMARY KEY (`ITEMIDNO`);

--
-- Indexes for table `tblofficialreceipt`
--
ALTER TABLE `tblofficialreceipt`
  ADD PRIMARY KEY (`ORNO`);

--
-- Indexes for table `tblstockin`
--
ALTER TABLE `tblstockin`
  ADD PRIMARY KEY (`SIIDNO`);

--
-- Indexes for table `tblstockout`
--
ALTER TABLE `tblstockout`
  ADD PRIMARY KEY (`SOIDNO`);

--
-- Indexes for table `tblstocks`
--
ALTER TABLE `tblstocks`
  ADD PRIMARY KEY (`SIIDNO`);

--
-- Indexes for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  ADD PRIMARY KEY (`SIDNO`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`USERIDNO`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblitem`
--
ALTER TABLE `tblitem`
  MODIFY `ITEMIDNO` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tblofficialreceipt`
--
ALTER TABLE `tblofficialreceipt`
  MODIFY `ORNO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tblstockin`
--
ALTER TABLE `tblstockin`
  MODIFY `SIIDNO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tblstockout`
--
ALTER TABLE `tblstockout`
  MODIFY `SOIDNO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tblstocks`
--
ALTER TABLE `tblstocks`
  MODIFY `SIIDNO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  MODIFY `SIDNO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `USERIDNO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
